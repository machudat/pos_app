package com.dat.pos.model;

public class InvoicesItem {
    private ProductItem productItem;
    private int count;

    public InvoicesItem(ProductItem productItem, int count) {
        this.productItem = productItem;
        this.count = count;
    }

    public InvoicesItem() {
    }

    public ProductItem getProductItem() {
        return productItem;
    }

    public void setProductItem(ProductItem productItem) {
        this.productItem = productItem;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    @Override
    public String toString() {
        return "InvoicesItem{" +
                "productItem=" + productItem +
                ", count=" + count +
                '}';
    }
}
