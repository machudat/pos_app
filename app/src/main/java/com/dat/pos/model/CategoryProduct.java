package com.dat.pos.model;

import java.util.List;

public class CategoryProduct {
    private String name;
    private List<ProductItem> ListProduct;
    private String imagePath;
    private String key;

    public CategoryProduct() {
    }

    public CategoryProduct(String name, List<ProductItem> ListProduct, String imagePath) {
        this.name = name;
        this.ListProduct = ListProduct;
        this.imagePath = imagePath;
    }

    public CategoryProduct(String name) {
        this.name = name;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductItem> getListProduct() {
        return ListProduct;
    }

    public void setListProduct(List<ProductItem> listProduct) {
        this.ListProduct = listProduct;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryProduct that = (CategoryProduct) o;
        return name.equals(that.name);
    }

}
