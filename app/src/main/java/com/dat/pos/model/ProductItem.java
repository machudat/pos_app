package com.dat.pos.model;

public class ProductItem {
    private String productName;
    private int price;
    private String note;
    private String ImagePath;
    private String key;
    private String nameCategory;
    private String keyCategory;

    public ProductItem(String productName, int price, String note, String ImagePath) {
        this.productName = productName;
        this.price = price;
        this.note = note;
        this.ImagePath = ImagePath;
    }

    public ProductItem(String productName, int price, String note, String imagePath, String nameCategory) {
        this.productName = productName;
        this.price = price;
        this.note = note;
        ImagePath = imagePath;
        this.nameCategory = nameCategory;
    }

    public ProductItem() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNameCategory() {
        return nameCategory;
    }

    public void setNameCategory(String nameCategory) {
        this.nameCategory = nameCategory;
    }

    public String getKeyCategory() {
        return keyCategory;
    }

    public void setKeyCategory(String keyCategory) {
        this.keyCategory = keyCategory;
    }

    public String getProductName() {
        return productName;
    }

    public String getImagePath() {
        return ImagePath;
    }

    public int getPrice() {
        return price;
    }

    public String getNote() {
        return note;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public void setImagePath(String imagePath) {
        ImagePath = imagePath;
    }

    @Override
    public String toString() {
        return "ProductItem{" +
                "productName='" + productName + '\'' +
                ", price=" + price +
                ", note='" + note + '\'' +
                ", ImagePath='" + ImagePath + '\'' +
                '}';
    }
}
