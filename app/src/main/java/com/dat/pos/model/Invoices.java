package com.dat.pos.model;

import java.util.ArrayList;
import java.util.List;

public class Invoices {
    private List<InvoicesItem> invoicesItem;
    private String id;
    private String timeCreated;
    private String term;
    private String name;
    private Customer customer;
    private String key;

    public Invoices() {
        invoicesItem = new ArrayList<>();
        customer = new Customer();
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Invoices(List<InvoicesItem> invoicesItem, String id, String timeCreated, String term, String name, Customer customer) {
        this.invoicesItem = invoicesItem;
        this.id = id;
        this.timeCreated = timeCreated;
        this.term = term;
        this.name = name;
        this.customer = customer;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTimeCreated() {
        return timeCreated;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTimeCreated(String timeCreated) {
        this.timeCreated = timeCreated;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public List<InvoicesItem> getInvoicesItem() {
        return invoicesItem;
    }

    public void setInvoicesItem(List<InvoicesItem> invoicesItem) {
        this.invoicesItem = invoicesItem;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }
}
