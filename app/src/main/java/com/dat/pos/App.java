package com.dat.pos;

import android.app.Application;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.dat.pos.ultils.FireBaseDatabaseHelp;
import com.dat.pos.ultils.RandomColor;
import com.dat.pos.ultils.StorageCommon;
import com.dat.pos.view.adapter.AnimAdapter;

public class App extends Application {
    private static App instance;
    private StorageCommon storage;
    private Animation animClick;
    private FireBaseDatabaseHelp firebaseDatabase;
    private RandomColor randomColor;

    public static App getInstance() {
        return instance;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        storage = new StorageCommon();
        firebaseDatabase = new FireBaseDatabaseHelp();
        randomColor = new RandomColor();
        animClick = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.anim_click);
        animClick.setAnimationListener(new AnimAdapter() {
            @Override
            public void onAnimationStart(Animation animation) {
                super.onAnimationStart(animation);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                super.onAnimationEnd(animation);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                super.onAnimationRepeat(animation);
            }
        });
    }

    public Animation getAnimation() {
        return animClick;
    }

    public StorageCommon getStorage() {
        return storage;
    }

    public FireBaseDatabaseHelp getFirebaseDatabase() {
        return firebaseDatabase;
    }

    public String getRandomColor() {
        return randomColor.getRDColor();
    }

}
