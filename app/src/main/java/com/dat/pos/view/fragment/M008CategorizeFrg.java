package com.dat.pos.view.fragment;

import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.R;
import com.dat.pos.model.CategoryProduct;
import com.dat.pos.presenter.M008CategorizePresenter;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.adapter.M008CategoryAdapter;
import com.dat.pos.view.dialog.M008DialogNewCategorize;

import java.util.ArrayList;
import java.util.List;

public class M008CategorizeFrg extends BaseFragment<M008CategorizePresenter> implements OnActionCallback {
    public static final String TAG = M008CategorizeFrg.class.getName();
    private RecyclerView rvCategory;
    private TextView imFix;
    private List<CategoryProduct> listCategory;
    private M008CategoryAdapter adapter;

    @Override
    protected void initView() {
        findViewById(R.id.cv_new_categorize, this);
        findViewById(R.id.m008_im_back, this);
        rvCategory = findViewById(R.id.m008_rv_category);
        imFix = findViewById(R.id.m008_tv_fix);

        rvCategory.setLayoutManager(new GridLayoutManager(mContext, 2));
        imFix.setHeight(imFix.getWidth());
    }

    @Override
    protected M008CategorizePresenter getPresenter() {
        return new M008CategorizePresenter(this);
    }

    @Override
    protected void initData() {
        listCategory = new ArrayList<>();
        getStorage().getListCategory().observe(this, new Observer<List<CategoryProduct>>() {
            @Override
            public void onChanged(List<CategoryProduct> item) {
                listCategory = item;
                adapter.setData(listCategory);
            }
        });
        adapter = new M008CategoryAdapter(this, listCategory, mContext);
        rvCategory.setAdapter(adapter);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.cv_new_categorize:
                showDialog();
                break;
            case R.id.m008_im_back:
                getActivity().onBackPressed();
                break;

        }
    }

    private void showDialog() {
        DialogFragment dialogFragment = M008DialogNewCategorize.newInstance(this);
        dialogFragment.show(getFragmentManager(), "New Category");
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case M008DialogNewCategorize.ADD_CATEGORIZE:
                List<CategoryProduct> listCategory = getStorage().getListCategory().getValue();
                getFireBaseDatabaseHelp().addNewCategory((String) data);
                break;
            case M008CategoryAdapter.SHOW_CATEGORY_DETAIL:
                CategoryProduct product = (CategoryProduct) data;
                break;
            case M008CategoryAdapter.DELETE_CATEGORY:
                getFireBaseDatabaseHelp().removeItemCategory((CategoryProduct) data);
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m008_frg_categorize;
    }
}
