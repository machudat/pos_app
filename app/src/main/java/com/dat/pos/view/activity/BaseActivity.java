package com.dat.pos.view.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.fragment.BaseFragment;

import java.util.Stack;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener, OnActionCallback {
    private String currentTag;
    private Fragment currentFragment;
    private Stack<Fragment> fragmentStack;

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        fragmentStack = new Stack<>();
        initView();
    }

    public final <T extends View> T findViewById(int id,
                                                 View.OnClickListener event) {
        T view = super.findViewById(id);
        if (event != null) {
            view.setOnClickListener(event);
        }
        return view;
    }

    protected abstract void initView();

    protected abstract int getLayoutId();

    @Override
    public void onClick(View v) {
        //do nothing
    }

    protected void showFrg(String tag, int add) {
        try {
            Class<?> clazz = Class.forName(tag);
            BaseFragment fragment = (BaseFragment) clazz.newInstance();
            fragment.setCallBack(this);
            currentTag = tag;
            currentFragment = fragment;
            FragmentManager frgMgr = getSupportFragmentManager();
            frgMgr.beginTransaction()
                    .replace(add, fragment, tag)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void addFrg(String tag, int add) {
        try {
            Class<?> clazz = Class.forName(tag);
            BaseFragment fragment = (BaseFragment) clazz.newInstance();
            fragment.setCallBack(this);
            currentTag = tag;
            currentFragment = fragment;
            fragmentStack.push(currentFragment);
            FragmentManager frgMgr = getSupportFragmentManager();
            frgMgr.beginTransaction()
                    .add(add, fragment, tag)
                    .commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void removeCurrentFrg() {
        getSupportFragmentManager().beginTransaction().remove(fragmentStack.pop()).commit();

    }

    @Override
    public void callBack(String key, Object data) {

    }

    public Fragment getCurrentFragment() {
        return currentFragment;
    }
}
