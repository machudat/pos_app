package com.dat.pos.view.fragment;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;

import com.dat.pos.R;
import com.dat.pos.model.UserInformationItem;
import com.dat.pos.presenter.M005ProfilePresenter;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.dialog.DialogInput;
import com.dat.pos.view.dialog.DialogPickGender;

public class M005ProfileFrg extends BaseFragment<M005ProfilePresenter> implements OnActionCallback {
    private static final String TAG = M005ProfileFrg.class.getName();
    public static final String CHANGE_NAME = "Đổi tên người dùng";
    private static final String EDIT_NAME = "edit game";
    private static final String PICK_GENDER = "Chọn giới tính";
    private static final String CHANGE_LOCATION = "Nhập địa chỉ";
    private static final String CHANGE_BIRTHDAY = "Cập nhật ngày sinh";
    private TextView tvEmail, tvName, tvGender, tvLocation, tvBirthday;
    public static final String KEY_SIGN_OUT_SUCCESS = "logout";
    private UserInformationItem userInformationItem;
    private int mYear, mMonth, mDay;

    public M005ProfileFrg(OnActionCallback callback) {
        this.mCallBack = callback;
    }

    @Override
    protected void initView() {
        findViewById(R.id.m005_setting_profile, this::onClick);
        findViewById(R.id.m005_setting, this::onClick);
        findViewById(R.id.m005_bt_sign_out, this::onClick);
        findViewById(R.id.m005_help, this::onClick);
        tvBirthday = findViewById(R.id.m005_tv_birthday, this::onClick);
        tvEmail = findViewById(R.id.m005_tv_email, this::onClick);
        tvGender = findViewById(R.id.m005_tv_gender, this::onClick);
        tvLocation = findViewById(R.id.m005_tv_location, this::onClick);
        tvName = findViewById(R.id.m005_tv_name, this::onClick);
        View.OnClickListener click = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "onClick: ");
            }
        };
        tvEmail.setOnClickListener(click);
        tvBirthday.setOnClickListener(click);
    }

    @Override
    protected M005ProfilePresenter getPresenter() {
        return new M005ProfilePresenter(this);
    }


    @Override
    protected void initData() {
        userInformationItem = new UserInformationItem();
        getStorage().getUserInformation().observe(this, new Observer<UserInformationItem>() {

            @Override
            public void onChanged(UserInformationItem item) {
                userInformationItem = new UserInformationItem();
                userInformationItem = item;
                if (userInformationItem.getEmail() != null) {
                    tvEmail.setText(item.getEmail());
                }
                if (userInformationItem.getName() != null) {
                    tvName.setText(item.getName());
                }
                if (userInformationItem.getGender() != null) {
                    tvGender.setText(item.getGender());
                }
                if (userInformationItem.getLocation() != null) {
                    tvLocation.setText(item.getLocation());
                }
                if (userInformationItem.getBirthday() != null) {
                    tvBirthday.setText(item.getBirthday());
                }
            }

        });
        String date = mPresenter.getCurrentDate();
        String[] splitDate = date.split("/");
        mDay = Integer.parseInt(splitDate[0]);
        mMonth = Integer.parseInt(splitDate[1]);
        mYear = Integer.parseInt(splitDate[2]);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m005_frg_profile;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.m005_bt_sign_out:
                getFireBaseDatabaseHelp().logout(mCallBack);
                break;
            case R.id.m005_tv_name:
                showDialogInput(CHANGE_NAME);
                break;
            case R.id.m005_tv_gender:
                showDialogPick(PICK_GENDER);
                break;
            case R.id.m005_tv_location:
                showDialogInput(CHANGE_LOCATION);
                break;
            case R.id.m005_tv_birthday:
                pickDate();

                break;
        }
    }

    private void pickDate() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                (view, year, monthOfYear, dayOfMonth) -> tvBirthday.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
        datePickerDialog.show();
        datePickerDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
                callBack(CHANGE_BIRTHDAY, tvBirthday.getText().toString());
            }
        });
    }

    private void showDialogPick(String key) {
        if (key.equals(PICK_GENDER)) {
            DialogFragment dialogFragment = new DialogPickGender(this, key);
            dialogFragment.show(getFragmentManager(), key);
        }
    }

    private void showDialogInput(String type) {
        DialogFragment dialogFragment = new DialogInput(this, type);
        dialogFragment.show(getFragmentManager(), type);
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case CHANGE_NAME:
                userInformationItem.setName((String) data);
                break;
            case PICK_GENDER:
                userInformationItem.setGender((String) data);
                break;
            case CHANGE_LOCATION:
                userInformationItem.setLocation((String) data);
                break;
            case CHANGE_BIRTHDAY:
                userInformationItem.setBirthday((String) data);
        }
        update();
    }

    private void update() {
        getFireBaseDatabaseHelp().updateProfile(userInformationItem);
    }
}
