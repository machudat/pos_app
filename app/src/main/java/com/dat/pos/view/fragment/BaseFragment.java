package com.dat.pos.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.dat.pos.App;
import com.dat.pos.presenter.BasePresenter;
import com.dat.pos.ultils.FireBaseDatabaseHelp;
import com.dat.pos.ultils.StorageCommon;
import com.dat.pos.view.Callback.OnActionCallback;

public abstract class BaseFragment<T extends BasePresenter> extends Fragment implements View.OnClickListener {
    protected Context mContext;
    protected View mRootView;
    protected OnActionCallback mCallBack;
    protected T mPresenter;

    protected abstract void initView();

    protected abstract T getPresenter();

    protected abstract void initData();

    public final void setCallBack(OnActionCallback mCallBack) {
        this.mCallBack = mCallBack;
    }

    @Override
    public final void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Nullable
    @Override
    public final View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutId(), container, false);
        mPresenter = getPresenter();
        initView();
        initData();
        return mRootView;
    }


    protected final <T extends View> T findViewById(@IdRes int id,
                                                    View.OnClickListener event) {
        T v = mRootView.findViewById(id);
        if (event != null) {
            v.setOnClickListener(event);
        }
        return v;
    }

    protected final <T extends View> T findViewById(@IdRes int id) {
        return mRootView.findViewById(id);
    }

    protected abstract int getLayoutId();


    public StorageCommon getStorage() {
        return App.getInstance().getStorage();
    }

    public Animation getAnimation() {
        return App.getInstance().getAnimation();
    }

    public FireBaseDatabaseHelp getFireBaseDatabaseHelp() {
        return App.getInstance().getFirebaseDatabase();
    }

    @Override
    public void onClick(View view) {
        try {
            view.startAnimation(getAnimation());
        } catch (Exception e) {
            Toast.makeText(mContext, "lỗi load anim", Toast.LENGTH_LONG).show();
        }
    }

}
