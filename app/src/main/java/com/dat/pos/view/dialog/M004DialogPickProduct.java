package com.dat.pos.view.dialog;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.App;
import com.dat.pos.R;
import com.dat.pos.model.CategoryProduct;
import com.dat.pos.model.InvoicesItem;
import com.dat.pos.model.ProductItem;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.adapter.M006ProductAdapter;
import com.google.android.material.textfield.TextInputEditText;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class M004DialogPickProduct extends BaseDialogFragment implements OnActionCallback {

    private static final String TAG = M004DialogPickProduct.class.getName();
    public static final String ADD_PRODUCT = "ADD_PRODUCT_TO_INVOICES";
    private List<ProductItem> productItemList;
    private M006ProductAdapter adapter;
    private RecyclerView recyclerView;
    private TextView tvNameProduct, tvTotalPrice;
    private TextInputEditText edtCount;
    private ProductItem item;

    public M004DialogPickProduct(OnActionCallback mCallback) {
        super(mCallback);
    }

    public static M004DialogPickProduct newInstance(OnActionCallback mCallback) {
        return new M004DialogPickProduct(mCallback);
    }

    @Override
    protected void initView() {
        findViewById(R.id.m004_minus, this::onClick);
        findViewById(R.id.m004_add, this::onClick);
        findViewById(R.id.m004_im_back_dialog, this::onClick);
        findViewById(R.id.m004_done_pick, this::onClick);
        edtCount = findViewById(R.id.m004_product_count_add);
        edtCount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().isEmpty() || Integer.parseInt(charSequence.toString()) <= 0 || Integer.parseInt(charSequence.toString()) > 100) {
                    edtCount.setText("1");
                    return;
                }
                calMoney();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        tvNameProduct = findViewById(R.id.m004_tv_name_product);
        tvTotalPrice = findViewById(R.id.m004_total_money2);
        recyclerView = findViewById(R.id.m004_rv_product_pick);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));
        productItemList = new ArrayList<>();
        App.getInstance().getStorage().getListCategory().observe(this, new Observer<List<CategoryProduct>>() {
            @Override
            public void onChanged(List<CategoryProduct> item) {
                List<ProductItem> newData = new ArrayList<>();
                for (CategoryProduct data : item) {
                    if (data.getListProduct() != null) {
                        for (ProductItem productItem : data.getListProduct()) {
                            newData.add(productItem);
                        }
                    }
                }
                productItemList = newData;
                adapter.setData(productItemList);
            }
        });
        adapter = new M006ProductAdapter(this, productItemList, mContext);
        recyclerView.setAdapter(adapter);
    }

    private void calMoney() {
        if (item == null) {
            return;
        }
        int money = (item.getPrice() * Integer.parseInt(edtCount.getText().toString()));
        Locale locale = new Locale("vi", "VN");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        tvTotalPrice.setText(numberFormat.format(money));
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m004_dialog_pick_product;
    }



    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.m004_add:
                edtCount.setText(String.valueOf((Integer.parseInt(edtCount.getText().toString()) + 1)));
                break;
            case R.id.m004_minus:
                edtCount.setText(String.valueOf((Integer.parseInt(edtCount.getText().toString()) - 1)));
                break;
            case R.id.m004_im_back_dialog:
                dismiss();
                break;
            case R.id.m004_done_pick:
                if (item == null) {
                    Toast.makeText(mContext, "Bạn chưa chọn sản phẩm", Toast.LENGTH_SHORT).show();
                    return;
                }
                mCallback.callBack(ADD_PRODUCT, new InvoicesItem(item, Integer.parseInt(edtCount.getText().toString())));
                dismiss();
                break;
        }
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case M006ProductAdapter.CALL_BACK_PRODUCT_CLICK:
                item = (ProductItem) data;
                Log.d(TAG, "callBack: " + item.getProductName());
                tvNameProduct.setText(item.getProductName());
                calMoney();
                break;
        }
    }
}
