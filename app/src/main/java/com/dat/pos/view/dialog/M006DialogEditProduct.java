package com.dat.pos.view.dialog;

import android.os.Build;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;

import com.dat.pos.App;
import com.dat.pos.R;
import com.dat.pos.model.CategoryProduct;
import com.dat.pos.model.ProductItem;
import com.dat.pos.view.Callback.OnActionCallback;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;
import java.util.Locale;

public class M006DialogEditProduct extends BaseDialogFragment {
    public static final String SAVE_INFORMATION = "Save";
    private static final String TAG = "edit product";
    private TextInputEditText edtName, editPrice, editNote;
    private ImageView imProduct;
    private AutoCompleteTextView dropdown;
    private final ProductItem data;
    private String[] listCate;
    private ArrayAdapter<String> dropdownAdapter;

    public M006DialogEditProduct(OnActionCallback mCallback, ProductItem data) {
        super(mCallback);
        this.data = data;
    }

    public static M006DialogEditProduct newInstance(OnActionCallback mCallback, ProductItem data) {
        return new M006DialogEditProduct(mCallback, data);
    }


    @RequiresApi(api = Build.VERSION_CODES.R)
    @Override
    protected void initView() {
        Locale locale = new Locale("vi", "VN");
        edtName = findViewById(R.id.m006_edit_product_name);
        editPrice = findViewById(R.id.m006_edit_product_price);
        editNote = findViewById(R.id.m006_edit_product_note);
        imProduct = findViewById(R.id.m006_edit_product_image, this);
        dropdown = findViewById(R.id.m006_dropdown_category);
        findViewById(R.id.m006_dialog_cancel, this);
        findViewById(R.id.m006_dialog_save, this);
        initData();

    }

    private void initData() {
        listCate = new String[0];
        App.getInstance().getStorage().getListCategory().observe(this, new Observer<List<CategoryProduct>>() {
            @Override
            public void onChanged(List<CategoryProduct> categoryProducts) {
                listCate = new String[categoryProducts.size()];
                if (categoryProducts.size() > 0) {
                    for (int i = 0; i < categoryProducts.size(); i++) {
                        listCate[i] = categoryProducts.get(i).getName();
                    }
                    dropdownAdapter = new ArrayAdapter<>(mContext, R.layout.dropdown_menu_item, listCate);
                    dropdown.setAdapter(dropdownAdapter);
                }

            }
        });
        dropdownAdapter = new ArrayAdapter<>(mContext, R.layout.dropdown_menu_item, listCate);
        dropdown.setAdapter(dropdownAdapter);
        App.getInstance().getFirebaseDatabase().downImage(data.getImagePath(), imProduct, mContext);
        edtName.setText(data.getProductName());
        editPrice.setText(String.valueOf(data.getPrice()));
        editNote.setText(data.getNote());
        dropdown.setText(data.getNameCategory());
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.m006_dialog_cancel:
                dismiss();
                break;
            case R.id.m006_dialog_save:
                //TODO thêm phần check
                data.setProductName(edtName.getText().toString());
                data.setNote(editNote.getText().toString());
                data.setPrice(Integer.parseInt(editPrice.getText().toString()));
                mCallback.callBack(SAVE_INFORMATION, data);
                dismiss();
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m006_dialog_product_detail;
    }
}
