package com.dat.pos.view.fragment;

import android.util.Log;
import android.view.View;

import androidx.cardview.widget.CardView;

import com.dat.pos.R;
import com.dat.pos.presenter.M003HomePresenter;
import com.dat.pos.view.Callback.OnActionCallback;

public class M003HomeFrg extends BaseFragment<M003HomePresenter> implements OnActionCallback {
    public static final String TAG = M003HomeFrg.class.getName();
    public static final String KEY_MOVE_TO_PRODUCT = "KEY_MOVE_TO_PRODUCT";
    public static final String MOVE_TO_CATEGORY = "Move to categorize";
    private CardView cvProduct, cvCategory;

    public M003HomeFrg(OnActionCallback callback) {
        this.mCallBack = callback;
    }

    @Override
    protected void initView() {
        Log.d(TAG, "initView: ");
        cvProduct = findViewById(R.id.m003_cv_product, this::onClick);
        cvCategory = findViewById(R.id.m003_cv_category, this::onClick);
    }

    @Override
    protected M003HomePresenter getPresenter() {
        return new M003HomePresenter(this);
    }

    @Override
    protected void initData() {
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.m003_cv_product:
                mCallBack.callBack(KEY_MOVE_TO_PRODUCT, null);
                break;
            case R.id.m003_cv_category:
                mCallBack.callBack(MOVE_TO_CATEGORY, null);
                break;
        }
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m003_frg_home;
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {

        }
    }


}
