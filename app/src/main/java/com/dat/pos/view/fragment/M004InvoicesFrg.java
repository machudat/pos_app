package com.dat.pos.view.fragment;

import android.view.View;
import android.widget.TextView;

import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.R;
import com.dat.pos.model.Invoices;
import com.dat.pos.presenter.M004InvoicesPresenter;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.adapter.M004InvoicesAdapter;

import java.util.ArrayList;
import java.util.List;

public class M004InvoicesFrg extends BaseFragment<M004InvoicesPresenter> implements OnActionCallback {
    public static final String TAG = M004InvoicesFrg.class.getName();
    public static final String SHOW_ADD_NEW_INVOICES = "SHOW INVOICES";
    public static final String SHOW_EDIT_INVOICES = "SHOW EDIT INVOICES";
    private List<Invoices> data;
    private M004InvoicesAdapter adapter;
    private RecyclerView recyclerView;
    private TextView tvTodayIncome, tvInvoices;

    public M004InvoicesFrg(OnActionCallback callback) {
        this.mCallBack = callback;
    }

    @Override
    protected void initView() {
        findViewById(R.id.m004_im_add_invoices, this);
        findViewById(R.id.m004_im_add_invoices, this);
        tvTodayIncome = findViewById(R.id.m004_today_income);
        tvInvoices = findViewById(R.id.m004_invoices_left);
        recyclerView = findViewById(R.id.m004_rv_invoices, this);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    protected M004InvoicesPresenter getPresenter() {
        return new M004InvoicesPresenter(this);
    }

    @Override
    protected void initData() {
        data = new ArrayList<>();
        getStorage().getListInvoices().observe(this, new Observer<List<Invoices>>() {
            @Override
            public void onChanged(List<Invoices> invoices) {
                data = invoices;
                adapter.setData(data);
                tvTodayIncome.setText(mPresenter.formatMoney(mPresenter.calculateTodayIncome(data)));
                tvInvoices.setText(mPresenter.calculateInvoicesLeft(data));
            }
        });
        adapter = new M004InvoicesAdapter(this, data, mContext);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m004_frg_invoices;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.m004_im_add_invoices:
                mCallBack.callBack(SHOW_ADD_NEW_INVOICES, null);
                break;
        }
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case M004InvoicesAdapter.REMOVE_INVOICES:
                getFireBaseDatabaseHelp().removeItemInvoices((Invoices) data);
                break;
            case M004InvoicesAdapter.EDIT_INVOICES:
                getStorage().setM004Invoices((Invoices) data);
                mCallBack.callBack(SHOW_EDIT_INVOICES, null);
                break;
        }
    }
}