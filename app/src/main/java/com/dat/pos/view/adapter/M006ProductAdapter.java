package com.dat.pos.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.App;
import com.dat.pos.R;
import com.dat.pos.model.ProductItem;
import com.dat.pos.view.Callback.OnActionCallback;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class M006ProductAdapter extends RecyclerView.Adapter<M006ProductAdapter.ItemDataHolder> {
    public static final String TAG = M006ProductAdapter.class.getName();
    public static final String CALL_BACK_PRODUCT_CLICK = "product clicked";
    public static final String CALL_BACK_REMOVE_PRODUCT = "remove product";
    private final OnActionCallback listener;
    private List<ProductItem> dataList;
    private final Context context;

    public M006ProductAdapter(OnActionCallback callback, List<ProductItem> dataList, Context context) {
        this.listener = callback;
        this.dataList = dataList;
        this.context = context;
    }

    public void setData(List<ProductItem> productItemList) {
        dataList = productItemList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemDataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.m006_full_product_item, parent, false);
        return new ItemDataHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemDataHolder holder, int position) {

        ProductItem data = dataList.get(position);
        App.getInstance().getFirebaseDatabase().downImage(data.getImagePath(), holder.imTopicImage, context);
        holder.tvProductName.setText(data.getProductName());
        holder.tvProductName.setTag(data);
        holder.tvPrice.setText(formatMoney(data.getPrice()));


    }

    public String formatMoney(int totalMoney) {
        Locale locale = new Locale("vi", "VN");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(totalMoney);
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ItemDataHolder extends RecyclerView.ViewHolder {
        TextView tvProductName, tvPrice;
        ImageView imTopicImage, imDelete, imDetail;

        public ItemDataHolder(@NonNull View itemView) {
            super(itemView);
            tvProductName = itemView.findViewById(R.id.m006_name_product);
            imTopicImage = itemView.findViewById(R.id.m006_im_product);
            imDelete = itemView.findViewById(R.id.m006_im_delete);
            imDetail = itemView.findViewById(R.id.m006_im_detail);
            tvPrice = itemView.findViewById(R.id.m006_tv_price);
            imTopicImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "onClick: ");
                    listener.callBack(CALL_BACK_PRODUCT_CLICK, tvProductName.getTag());
                }
            });
            imDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.callBack(CALL_BACK_REMOVE_PRODUCT, tvProductName.getTag());
                }
            });
            imDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.callBack(CALL_BACK_PRODUCT_CLICK, tvProductName.getTag());
                }
            });
        }
    }
}
