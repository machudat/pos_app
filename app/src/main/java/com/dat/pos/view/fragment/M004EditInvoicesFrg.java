package com.dat.pos.view.fragment;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.R;
import com.dat.pos.model.Invoices;
import com.dat.pos.model.InvoicesItem;
import com.dat.pos.presenter.M004AddInvoicesPresenter;
import com.dat.pos.ultils.FireBaseDatabaseHelp;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.adapter.M004ProductAdapter;
import com.dat.pos.view.dialog.M004DialogPickProduct;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

public class M004EditInvoicesFrg extends BaseFragment<M004AddInvoicesPresenter> implements OnActionCallback {
    public static final String TAG = M004EditInvoicesFrg.class.getName();
    private Invoices invoices;
    private M004ProductAdapter adapter;
    private RecyclerView recyclerView;
    private TextView tvTotalMoney, tvTime, tvID;
    private TextInputEditText edtTitle;
    private int mYear, mMonth, mDay;
    private M004DialogPickProduct dialogPickProduct;

    @Override
    protected void initView() {
        recyclerView = findViewById(R.id.m004_dialog_edit_rv_product);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        tvTotalMoney = findViewById(R.id.m004_dialog_edit_total_money);
        tvTime = findViewById(R.id.m004_dialog_edit_tv_time);
        tvID = findViewById(R.id.m004_dialog_edit_add_id);
        edtTitle = findViewById(R.id.m004_dialog_edit_add_title);

        findViewById(R.id.m004_dialog_edit_add_product, this::onClick);
        findViewById(R.id.m004_dialog_edit_done, this::onClick);
        findViewById(R.id.m004_dialog_edit_pick_date, this::onClick);
        findViewById(R.id.m004_dialog_edit_back, this::onClick);
    }

    @Override
    protected M004AddInvoicesPresenter getPresenter() {
        return new M004AddInvoicesPresenter(this);
    }

    @Override
    protected void initData() {
        invoices = getStorage().getM004Invoices();
        tvTime.setText(invoices.getTimeCreated());
        edtTitle.setText(invoices.getName());
        tvID.setText(invoices.getId());
        adapter = new M004ProductAdapter(this, invoices.getInvoicesItem(), mContext);
        recyclerView.setAdapter(adapter);
        tvID.setText(String.format("%05d", Integer.parseInt(invoices.getId())));
        tvTotalMoney.setText(String.valueOf(mPresenter.calculateToTalMoney(invoices)));
        tvTime.setText(invoices.getTerm());
        String date = invoices.getTerm();
        String[] splitDate = date.split("/");
        mDay = Integer.parseInt(splitDate[0]);
        mMonth = Integer.parseInt(splitDate[1]);
        mYear = Integer.parseInt(splitDate[2]);
        setData();
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.m004_dialog_edit_pick_date:
                pickDate();
                break;
            case R.id.m004_dialog_edit_add_product:
                showDialog();
                break;
            case R.id.m004_dialog_edit_back:
                getActivity().onBackPressed();
                break;
            case R.id.m004_dialog_edit_done:
                setData();
                if (mPresenter.update(invoices)) {
                    getActivity().onBackPressed();
                } else {
                    Snackbar.make(mRootView, "Cần thêm thông tin", BaseTransientBottomBar.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void setData() {
        invoices.setId(tvID.getText().toString());
        invoices.setName(Objects.requireNonNull(edtTitle.getText()).toString());
        invoices.setTerm(tvTime.getText().toString());
    }

    private void showDialog() {
        dialogPickProduct = M004DialogPickProduct.newInstance(this);
        dialogPickProduct.show(getFragmentManager(), "add product to invoices");

    }

    private void pickDate() {
        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                (view, year, monthOfYear, dayOfMonth) -> tvTime.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.m004_frg_edit_invoices;
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case M004DialogPickProduct.ADD_PRODUCT:
                invoices.getInvoicesItem().add((InvoicesItem) data);
                adapter.notifyDataSetChanged();
                tvTotalMoney.setText(mPresenter.formatMoney(mPresenter.calculateToTalMoney(invoices)));
                break;
            case FireBaseDatabaseHelp.SUCCESS:
                Snackbar.make(mRootView, "Cập nhật thành công", BaseTransientBottomBar.LENGTH_SHORT).show();
                break;
        }
    }
}
