package com.dat.pos.view.fragment;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.R;
import com.dat.pos.model.CategoryProduct;
import com.dat.pos.model.ProductItem;
import com.dat.pos.presenter.M006AllProductPresenter;
import com.dat.pos.ultils.FireBaseDatabaseHelp;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.adapter.M006ProductAdapter;
import com.dat.pos.view.dialog.M006DialogEditProduct;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

public class M006AllProductFrg extends BaseFragment<M006AllProductPresenter> implements OnActionCallback {
    public static final String TAG = M006AllProductFrg.class.getName();
    public static final String KEY_ADD_PRODUCT = "Go Add";
    public ImageView imBack, imAdd;
    private List<ProductItem> productItemList;
    private RecyclerView recyclerView;
    private M006ProductAdapter adapter;

    @Override
    protected void initView() {
        recyclerView = findViewById(R.id.m006_rv_view);
        imBack = findViewById(R.id.im_back, this);
        imAdd = findViewById(R.id.m006_im_add_product, this);
        recyclerView.setLayoutManager(new GridLayoutManager(mContext, 2));

    }

    @Override
    protected M006AllProductPresenter getPresenter() {
        return new M006AllProductPresenter(this);
    }

    @Override
    protected void initData() {
        productItemList = new ArrayList<>();
        getStorage().getListCategory().observe(this, new Observer<List<CategoryProduct>>() {
            @Override
            public void onChanged(List<CategoryProduct> item) {
                List<ProductItem> newData = new ArrayList<>();
                for (CategoryProduct data : item) {
                    Log.d(TAG, "onChanged: " + data.getName());
                    if (data.getListProduct() != null) {
                        for (ProductItem productItem : data.getListProduct()) {
                            newData.add(productItem);
                        }
                    }
                }
                productItemList = newData;
                adapter.setData(productItemList);
            }
        });
        adapter = new M006ProductAdapter(this, productItemList, mContext);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m006_frg_product;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.im_back:
                getActivity().onBackPressed();
                break;
            case R.id.m006_im_add_product:
                mCallBack.callBack(KEY_ADD_PRODUCT, null);
                break;
        }
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case M006ProductAdapter.CALL_BACK_PRODUCT_CLICK:
                showDialog((ProductItem) data);
                break;
            case M006DialogEditProduct.SAVE_INFORMATION:
                mPresenter.update((ProductItem) data, this);
                break;
            case FireBaseDatabaseHelp.UPDATE_SUCCESS:
                Snackbar.make(mRootView, "Cập nhật thành công", Snackbar.LENGTH_SHORT).show();
                break;
            case M006ProductAdapter.CALL_BACK_REMOVE_PRODUCT:
                if (mPresenter.removeItem((ProductItem) data)) {
                    Snackbar.make(mRootView, "Xoá thành công", Snackbar.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void showDialog(ProductItem data) {
        DialogFragment dialogFragment = M006DialogEditProduct.newInstance(this, data);
        dialogFragment.show(getFragmentManager(), "edit product");

    }
}
