package com.dat.pos.view.fragment;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.dat.pos.R;
import com.dat.pos.presenter.M001LoginPresenter;
import com.dat.pos.ultils.FireBaseDatabaseHelp;
import com.dat.pos.view.Callback.OnActionCallback;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;

public class M001LoginFrg extends BaseFragment<M001LoginPresenter> implements OnActionCallback {
    public static final String TAG = M001LoginFrg.class.getName();
    public static final String KEY_FORGOT_PASSWORD = "Quên mật khẩu";
    public static final String REGISTER = "Đăng ký tài khoản";
    public static final String LOGIN_SUCESS = "Đăng nhập thành công";
    private TextInputEditText emailEditText, passWordEditText;
    FirebaseAuth firebaseAuth;
    private Button btLogin;
    private TextView tvForgotpass, tvRegister;

    @Override
    protected M001LoginPresenter getPresenter() {
        return new M001LoginPresenter(this);
    }

    @Override
    protected void initData() {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m001_frg_login;
    }

    @Override
    protected void initView() {

        btLogin = findViewById(R.id.cirLoginButton, this);
        tvForgotpass = findViewById(R.id.tv_forgot_password, this);
        tvRegister = findViewById(R.id.tv_register, this);
        emailEditText = findViewById(R.id.editTextEmail);
        passWordEditText = findViewById(R.id.editTextPassword);

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {

            case R.id.tv_forgot_password:
                forgotPassword();
                break;
            case R.id.tv_register:
                register();
                break;
            case R.id.cirLoginButton:
                login();
                break;
        }
    }

    private void login() {
        String email = emailEditText.getText().toString().trim();
        String password = passWordEditText.getText().toString().trim();
        if (mPresenter.checkLogin(email, password)) {
            getFireBaseDatabaseHelp().login(this, email, password);
        } else {
            Toast.makeText(mContext, "Sai định dạng", Toast.LENGTH_SHORT).show();
        }
    }

    private void register() {
        mCallBack.callBack(REGISTER, null);
    }

    private void forgotPassword() {
        Toast.makeText(mContext, "Chức năng này chưa có ", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case FireBaseDatabaseHelp.SUCCESS:
                mCallBack.callBack(LOGIN_SUCESS, null);
                break;
            case FireBaseDatabaseHelp.FAIL:
                Toast.makeText(mContext, "Đăng nhập thất bại", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}
