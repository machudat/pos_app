package com.dat.pos.view.fragment;

import android.os.Build;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.Observer;

import com.anychart.APIlib;
import com.anychart.AnyChart;
import com.anychart.AnyChartView;
import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.anychart.charts.Cartesian;
import com.anychart.charts.Pie;
import com.anychart.core.cartesian.series.Column;
import com.dat.pos.R;
import com.dat.pos.model.Invoices;
import com.dat.pos.presenter.M007StatisticPresenter;
import com.dat.pos.view.Callback.OnActionCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class M007StatisticFrg extends BaseFragment<M007StatisticPresenter> implements OnActionCallback {
    private static final String TAG = M007StatisticFrg.class.getName();
    private Pie pie;
    private Cartesian cartesian;
    private List<DataEntry> dataCartesian, dataPie;
    private TextView tvTitle, tvTitle2;
    private AnyChartView chartViewCartesian, chartViewPie;
    private int mYear, mMonth, mDay;
    private final boolean chartIsUsing = true;

    public M007StatisticFrg(OnActionCallback mCallback) {
        super();
        this.mCallBack = mCallback;
    }

    @Override
    protected void initView() {
        tvTitle = findViewById(R.id.m007_tv_title, this);
        tvTitle2 = findViewById(R.id.m007_tv_title2, this);
        dataCartesian = new ArrayList<>();
        dataPie = new ArrayList<>();
        initPie();
        initCartesian();
    }


    private void initCartesian() {
        initCartesianData();
        chartViewCartesian = findViewById(R.id.m007_chart2);
        APIlib.getInstance().setActiveAnyChartView(chartViewCartesian);
        cartesian = AnyChart.column();
        cartesian.legend().fontSize("bold").fontFamily("roboto_slab_light").fontColor("#000");
        Column column = cartesian.column(dataCartesian);
        cartesian.data(dataCartesian);
        chartViewCartesian.setChart(cartesian);
    }

    private void initCartesianData() {
        dataCartesian = new ArrayList<>();
        setDays(6);
        setDays(5);
        setDays(4);
        setDays(3);
        setDays(2);
        setDays(1);
        setDays(0);
    }

    private void setDays(int i) {
        String sixDaysAgo = calDays(i);
        dataCartesian.add(new ValueDataEntry(sixDaysAgo, 0));
    }

    private void initPie() {
        chartViewPie = findViewById(R.id.m007_chart);
        APIlib.getInstance().setActiveAnyChartView(chartViewPie);
        pie = AnyChart.pie();
        dataPie.add(new ValueDataEntry("Chưa có thông tin", 1));
        pie.data(dataPie);
        chartViewPie.setChart(pie);
        pie.legend().position("center-top").fontSize("bold").fontFamily("roboto_slab_light").fontColor("#000");
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void initData() {
        getStorage().getListInvoices().observe(this, new Observer<List<Invoices>>() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onChanged(List<Invoices> invoices) {
                dataPie = mPresenter.getDataTemplatePie(invoices);
                dataCartesian = mPresenter.getDateTemplateColumn(dataCartesian, invoices);
                if (dataCartesian.isEmpty()) {
                    Log.d(TAG, "onChanged: dataCartesian is empty");
                    initCartesianData();
                }
                APIlib.getInstance().setActiveAnyChartView(chartViewCartesian);
                cartesian.data(dataCartesian);
                if (dataPie.isEmpty()) {
                    dataPie.add(new ValueDataEntry("Chưa có thông tin", 1));
                }
                APIlib.getInstance().setActiveAnyChartView(chartViewPie);
                pie.data(dataPie);
            }
        });
    }


    @Override
    protected M007StatisticPresenter getPresenter() {
        return new M007StatisticPresenter(this);
    }


    private String calDays(int i) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -i);
        Date date = c.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM");
        String strDate = dateFormat.format(date);
        return strDate;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m007_frg_statistic;
    }

}
