package com.dat.pos.view.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.fragment.M003HomeFrg;
import com.dat.pos.view.fragment.M004InvoicesFrg;
import com.dat.pos.view.fragment.M005ProfileFrg;
import com.dat.pos.view.fragment.M007StatisticFrg;

public class ContentMainAdapter extends FragmentStatePagerAdapter {
    private int mNumOfTabs;
    private OnActionCallback mCallback;
    public static final int HOME_FRG = 0;
    public static final int INVOICES = 1;
    public static final int STATISTICS = 2;
    public static final int PROFILE = 3;

    private Fragment[] fragments;

    public ContentMainAdapter(@NonNull FragmentManager fm, int numOfTabs, OnActionCallback callback) {
        super(fm);
        this.mNumOfTabs = numOfTabs;
        this.mCallback = callback;
        fragments = new Fragment[this.mNumOfTabs];
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case HOME_FRG:
                if (fragments[HOME_FRG] == null) {
                    fragments[HOME_FRG] = new M003HomeFrg(mCallback);
                }
                break;
            case INVOICES:
                if (fragments[INVOICES] == null) {
                    fragments[INVOICES] = new M004InvoicesFrg(mCallback);
                }
                break;
            case STATISTICS:
                if (fragments[STATISTICS] == null) {
                    fragments[STATISTICS] = new M007StatisticFrg(mCallback);
                }
            case PROFILE:
                if (fragments[PROFILE] == null) {
                    fragments[PROFILE] = new M005ProfileFrg(mCallback);
                }
                break;

        }
        return fragments[position];
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    public Fragment getFragment(int index) {
        return fragments[index];
    }

    public int getmNumOfTabs() {
        return mNumOfTabs;
    }
}
