package com.dat.pos.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.Observer;

import com.bumptech.glide.Glide;
import com.dat.pos.R;
import com.dat.pos.model.CategoryProduct;
import com.dat.pos.presenter.M007AddProductPresenter;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.dialog.M006DialogEditProduct;
import com.dat.pos.view.dialog.M008DialogNewCategorize;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

public class M006AddProductFrg extends BaseFragment<M007AddProductPresenter> implements OnActionCallback {
    public static final String TAG = M006AddProductFrg.class.getName();
    public static final int REQUEST_IMAGE = 111;
    private static final int CREATE_CATEGORY = 0;
    private ImageView imBack, imProductImage;
    private EditText edtName, edtPrice, edtNote;
    private AutoCompleteTextView dropdown;
    private ArrayAdapter dropdownAdapter;
    private String[] listCate;


    @Override
    protected void initView() {
        imBack = findViewById(R.id.m007_im_back, this);
        findViewById(R.id.m007_add, this);
        findViewById(R.id.m007_im_set_im_product, this);
        imProductImage = findViewById(R.id.m007_im_product);
        edtName = findViewById(R.id.edt_name);
        edtPrice = findViewById(R.id.edt_price);
        edtNote = findViewById(R.id.edt_note);
        dropdown = mRootView.findViewById(R.id.dropdown_category);

    }

    @Override
    protected M007AddProductPresenter getPresenter() {
        return new M007AddProductPresenter(this);
    }

    @Override
    protected void initData() {
        listCate = new String[1];
        listCate[CREATE_CATEGORY] = "Tạo chủ đề";
        getStorage().getListCategory().observe(this, new Observer<List<CategoryProduct>>() {
            @Override
            public void onChanged(List<CategoryProduct> categoryProducts) {
                listCate = new String[categoryProducts.size() + 1];
                listCate[CREATE_CATEGORY] = "Tạo chủ đề";
                Log.d(TAG, "onChanged: " + listCate[CREATE_CATEGORY]);
                for (int i = 0; i < categoryProducts.size(); i++) {
                    listCate[i + 1] = categoryProducts.get(i).getName();
                }
                dropdownAdapter = new ArrayAdapter<>(mContext, R.layout.dropdown_menu_item, listCate);
                dropdown.setAdapter(dropdownAdapter);
            }
        });
        dropdownAdapter = new ArrayAdapter<>(mContext, R.layout.dropdown_menu_item, listCate);
        dropdown.setAdapter(dropdownAdapter);

        dropdown.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i == CREATE_CATEGORY) {
                    dropdown.setText("");
                    DialogFragment dialogFragment = M008DialogNewCategorize.newInstance(new OnActionCallback() {
                        @Override
                        public void callBack(String key, Object data) {
                            String text = (String) data;
                            getFireBaseDatabaseHelp().addNewCategory(text);
                            dropdown.setText(text);
                        }
                    });
                    dialogFragment.show(getFragmentManager(), "New Category");
                }
            }
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m006_add_product;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.m007_im_set_im_product:
                pickImage();
                break;
            case R.id.m007_im_back:
                getActivity().onBackPressed();
                break;
            case R.id.m007_add:
                String name = edtName.getText().toString();
                String price = edtPrice.getText().toString();
                String note = edtNote.getText().toString();
                String category = dropdown.getText().toString();
                Uri uri = null;
                try {
                    uri = (Uri) imProductImage.getTag();
                } catch (IllegalArgumentException e) {

                }
                if (mPresenter.addProduct(category, name, price, note, uri)) {
                    getActivity().onBackPressed();
                } else {
                    Snackbar.make(mRootView, R.string.more_information, Snackbar.LENGTH_SHORT).show();
                }
                break;
        }
    }


    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case M006DialogEditProduct.SAVE_INFORMATION:
                getFireBaseDatabaseHelp().saveProductInformation(data);
                break;
        }
    }

    private void pickImage() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.setType("image/");
        Toast.makeText(mContext, "hey", Toast.LENGTH_SHORT).show();
        startActivityForResult(intent, M006AddProductFrg.REQUEST_IMAGE);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d(TAG, "onActivityResult: " + requestCode);
        if (resultCode == getActivity().RESULT_CANCELED) {
            Toast.makeText(mContext, "No thing to get!",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        if (data == null) return;
        if (requestCode == M006AddProductFrg.REQUEST_IMAGE) {
            Uri uriImage = data.getData();
            Glide.with(this).load(uriImage)
                    .placeholder(R.drawable.burger)
                    .into(imProductImage);
            imProductImage.setTag(uriImage);
        }
    }

}