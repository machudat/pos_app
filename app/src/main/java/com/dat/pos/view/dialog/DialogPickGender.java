package com.dat.pos.view.dialog;

import android.util.Log;
import android.view.View;
import android.widget.RadioButton;
import android.widget.TextView;

import com.dat.pos.R;
import com.dat.pos.view.Callback.OnActionCallback;

public class DialogPickGender extends BaseDialogFragment {
    private static final String TAG = DialogPickGender.class.getName();
    private String title;
    private RadioButton btMale, btFemale, btLGBT;
    private TextView tvTitle;
    private String genderPicked;

    public DialogPickGender(OnActionCallback mCallback, String title) {
        super(mCallback);
        this.title = title;
    }

    @Override
    protected void initView() {
        btMale = findViewById(R.id.radio_male, this::onRadioButtonClicked);
        btFemale = findViewById(R.id.radio_female, this::onRadioButtonClicked);
        btLGBT = findViewById(R.id.radio_lgbt, this::onRadioButtonClicked);
        tvTitle = findViewById(R.id.dialog_pick_gender_title);
        findViewById(R.id.dialog_pick_gender_confirm, this::onClick);
        tvTitle.setText(title);
        findViewById(R.id.dialog_pick_gender_back, this::onClick);
        genderPicked = btMale.getText().toString();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog_pick_gender;
    }

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();

        switch (view.getId()) {
            case R.id.radio_male:
                if (checked) {
                    btFemale.setChecked(false);
                    btLGBT.setChecked(false);
                    genderPicked = btMale.getText().toString();
                    Log.d(TAG, "onRadioButtonClicked: " + genderPicked);
                }
                break;
            case R.id.radio_female:
                if (checked) {
                    btMale.setChecked(false);
                    btLGBT.setChecked(false);
                    genderPicked = btFemale.getText().toString();
                }
                break;
            case R.id.radio_lgbt:
                if (checked) {
                    btMale.setChecked(false);
                    btFemale.setChecked(false);
                    genderPicked = btLGBT.getText().toString();
                }

                break;
        }
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.dialog_pick_gender_confirm:
                mCallback.callBack(tvTitle.getText().toString(), genderPicked);
                dismiss();
                break;
            case R.id.dialog_pick_gender_back:
                dismiss();
                break;
        }
    }
}
