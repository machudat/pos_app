package com.dat.pos.view.dialog;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dat.pos.R;
import com.dat.pos.view.Callback.OnActionCallback;

public class DialogInput extends BaseDialogFragment {

    private TextView tvTitle;
    private Button btConfirm;
    private final String title;
    private EditText edtEdit;

    public DialogInput(OnActionCallback mCallback, String title) {
        super(mCallback);
        this.title = title;
    }

    @Override
    protected void initView() {
        tvTitle = findViewById(R.id.dialog_title);
        btConfirm = findViewById(R.id.dialog_confirm, this::onClick);
        edtEdit = findViewById(R.id.dialog_edt);
        findViewById(R.id.dialog_back, this::onClick);
        tvTitle.setText(title);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.dialog;
    }


    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.dialog_confirm:
                if (!tvTitle.getText().toString().isEmpty()) {
                    mCallback.callBack(title, edtEdit.getText().toString());
                }
                dismiss();
                break;
            case R.id.dialog_back:
                dismiss();
                break;
        }
    }
}
