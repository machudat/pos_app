package com.dat.pos.view.dialog;

import android.view.View;
import android.widget.Toast;

import com.dat.pos.R;
import com.dat.pos.view.Callback.OnActionCallback;
import com.google.android.material.textfield.TextInputEditText;

public class M008DialogNewCategorize extends BaseDialogFragment {
    public static final String ADD_CATEGORIZE = "add categorize";
    private TextInputEditText edtNameCategorize;

    public M008DialogNewCategorize(OnActionCallback mCallback) {
        super(mCallback);
    }

    public static M008DialogNewCategorize newInstance(OnActionCallback mCallback) {
        return new M008DialogNewCategorize(mCallback);
    }

    @Override
    protected void initView() {
        findViewById(R.id.m008_bt_cancel, this::onClick);
        findViewById(R.id.m008_bt_accept, this::onClick);
        edtNameCategorize = findViewById(R.id.edt_name_categorize);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.m008_dialog_new_categorize;
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.m008_bt_cancel:
                dismiss();
                break;
            case R.id.m008_bt_accept:
                if (edtNameCategorize.getText().toString().isEmpty()) {
                    Toast.makeText(mContext, "Chưa nhập tên chủ đề", Toast.LENGTH_SHORT).show();
                    return;
                }
                mCallback.callBack(ADD_CATEGORIZE, edtNameCategorize.getText().toString());
                dismiss();
        }
    }

}
