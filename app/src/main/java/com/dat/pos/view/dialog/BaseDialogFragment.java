package com.dat.pos.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.dat.pos.App;
import com.dat.pos.R;
import com.dat.pos.view.Callback.OnActionCallback;

public abstract class BaseDialogFragment extends DialogFragment implements View.OnClickListener {
    protected Context mContext;
    protected OnActionCallback mCallback;
    private View mRootView;

    public BaseDialogFragment(OnActionCallback mCallback) {
        super();
        this.mCallback = mCallback;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public void onStart() {
        super.onStart();
        Dialog dialog = getDialog();
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
            dialog.getWindow().setWindowAnimations(R.style.dialog_anim);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        return super.onCreateDialog(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRootView = inflater.inflate(getLayoutId(), container, false);
        initView();
        return mRootView;
    }

    protected abstract void initView();

    protected final <T extends View> T findViewById(@IdRes int id,
                                                    View.OnClickListener event) {
        T v = mRootView.findViewById(id);
        if (event != null) {
            v.setOnClickListener(event);
        }
        return v;
    }

    protected final <T extends View> T findViewById(@IdRes int id) {
        return mRootView.findViewById(id);
    }

    protected abstract int getLayoutId();

    @Override
    public void onClick(View view) {
        try {
            view.startAnimation(App.getInstance().getAnimation());
        } catch (Exception e) {
            Toast.makeText(mContext, "lỗi load anim", Toast.LENGTH_LONG).show();
        }
    }
}
