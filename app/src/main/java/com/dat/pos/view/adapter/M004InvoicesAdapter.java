package com.dat.pos.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.R;
import com.dat.pos.model.Invoices;
import com.dat.pos.model.InvoicesItem;
import com.dat.pos.view.Callback.OnActionCallback;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class M004InvoicesAdapter extends RecyclerView.Adapter<M004InvoicesAdapter.ItemDataHolder> {

    private static final String TAG = M004InvoicesAdapter.class.getName();
    public static final String REMOVE_INVOICES = "xoa hoa don";
    public static final String EDIT_INVOICES = "Chỉnh sửa hoá đơn";
    private final Context mContext;
    private final OnActionCallback callBack;
    private List<Invoices> invoicesList;

    public M004InvoicesAdapter(OnActionCallback callback, List<Invoices> invoicesList, Context mContext) {
        this.invoicesList = invoicesList;
        this.mContext = mContext;
        this.callBack = callback;
    }

    @NonNull
    @Override
    public ItemDataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.m004_invoices_item, parent, false);
        return new M004InvoicesAdapter.ItemDataHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemDataHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder: " + position);
        Invoices item = invoicesList.get(position);
        int totalAmount = 0;
        for (InvoicesItem invoicesItem : item.getInvoicesItem()) {
            totalAmount += invoicesItem.getCount() * invoicesItem.getProductItem().getPrice();
        }
        holder.tvId.setTag(item);
        holder.tvPrice.setText((formatMoney(totalAmount)));
        holder.tvId.setText("# " + item.getId());
        holder.tvName.setText(item.getName());
        holder.tvTime.setText(item.getTimeCreated());
        holder.tvDate.setText(item.getTerm());
    }

    @Override
    public int getItemCount() {
        return invoicesList.size();
    }

    private String formatMoney(int totalMoney) {
        Locale locale = new Locale("vi", "VN");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(totalMoney);
    }

    public void setData(List<Invoices> data) {
        try {
            if (data.size() < invoicesList.size()) {
                for (int i = 1; i < invoicesList.size(); i++) {
                    if (invoicesList.get(i).getId().equals(data.get(i - 1).getId())) {
                        invoicesList.remove(i);
                        notifyItemRemoved(i);
                    }
                }
            }
            invoicesList = data;
        } catch (IndexOutOfBoundsException E) {
            data = new ArrayList<>();
        }

        notifyDataSetChanged();
    }

    public class ItemDataHolder extends RecyclerView.ViewHolder {
        TextView tvId, tvName, tvPrice, tvTime, tvDate;
        ImageView imDelete;

        public ItemDataHolder(@NonNull View itemView) {
            super(itemView);
            tvId = itemView.findViewById(R.id.m004_item_id);
            tvName = itemView.findViewById(R.id.m004_item_name);
            tvPrice = itemView.findViewById(R.id.m004_item_money);
            tvTime = itemView.findViewById(R.id.m004_item_date_create);
            tvDate = itemView.findViewById(R.id.m004_item_date);
            imDelete = itemView.findViewById(R.id.m004_im_delete_invoices);
            imDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int i = 0; i < invoicesList.size(); i++) {
                        Invoices item = invoicesList.get(i);
                        if (item.getId().equals(((Invoices) tvId.getTag()).getId())) {
                            callBack.callBack(REMOVE_INVOICES, invoicesList.get(i));
//                            Log.d(TAG, "onClick: remove");
//                            invoicesList.remove(i);
//                            notifyItemRemoved(i);

                            return;
                        }
                    }
                }
            });
            itemView.findViewById(R.id.m004_item_main).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    callBack.callBack(EDIT_INVOICES, tvId.getTag());
                }
            });
        }
    }


}
