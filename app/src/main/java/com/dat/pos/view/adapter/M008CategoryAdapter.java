package com.dat.pos.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.App;
import com.dat.pos.R;
import com.dat.pos.model.CategoryProduct;
import com.dat.pos.view.Callback.OnActionCallback;

import java.util.List;

public class M008CategoryAdapter extends RecyclerView.Adapter<M008CategoryAdapter.ItemDataHolder> {
    public static final String SHOW_CATEGORY_DETAIL = "SHOW_CATEGORY_DETAIL";
    private static final String TAG = M008CategoryAdapter.class.getName();
    public static final String DELETE_CATEGORY = "DELETE_CATEGORY";
    private final OnActionCallback listener;
    private List<CategoryProduct> dataList;
    private final Context context;

    public M008CategoryAdapter(OnActionCallback callback, List<CategoryProduct> dataList, Context context) {
        this.listener = callback;
        this.dataList = dataList;
        this.context = context;
    }

    public void setData(List<CategoryProduct> productItemList) {
        dataList = productItemList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ItemDataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(context).inflate(R.layout.m008_categorize_item, parent, false);
        return new ItemDataHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemDataHolder holder, int position) {
        CategoryProduct data = dataList.get(position);
        holder.tvCategoryName.setText(data.getName());
        App.getInstance().getFirebaseDatabase().downImage(data.getImagePath(), holder.imCategory, context);
        holder.tvCategoryName.setTag(data);
        holder.tvCategoryName.setTag(data);
        Log.d(TAG, "onBindViewHolder: " + data.getName());
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class ItemDataHolder extends RecyclerView.ViewHolder {
        TextView tvCategoryName;
        ImageView imCategory, imDelete;

        public ItemDataHolder(@NonNull View itemView) {
            super(itemView);
            tvCategoryName = itemView.findViewById(R.id.category_name);
            tvCategoryName.setBackgroundColor(Color.parseColor(App.getInstance().getRandomColor()));
            imCategory = itemView.findViewById(R.id.im_category);
            imDelete = itemView.findViewById(R.id.m008_im_delete);
            imDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.callBack(DELETE_CATEGORY, tvCategoryName.getTag());
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.callBack(SHOW_CATEGORY_DETAIL, tvCategoryName.getTag());
                }
            });
        }
    }

}
