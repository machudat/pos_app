package com.dat.pos.view.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.R;
import com.dat.pos.model.InvoicesItem;
import com.dat.pos.view.Callback.OnActionCallback;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class M004ProductAdapter extends RecyclerView.Adapter<M004ProductAdapter.ItemDataHolder> {

    private static final String TAG = M004ProductAdapter.class.getName();
    private final Context mContext;
    private final OnActionCallback callBack;
    private final List<InvoicesItem> listInvoicesItem;

    public M004ProductAdapter(OnActionCallback callback, List<InvoicesItem> listInvoicesItem, Context mContext) {
        this.listInvoicesItem = listInvoicesItem;
        this.mContext = mContext;
        this.callBack = callback;
    }

    @NonNull
    @Override
    public ItemDataHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.m004_item_product, parent, false);
        return new M004ProductAdapter.ItemDataHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemDataHolder holder, int position) {
        InvoicesItem item = listInvoicesItem.get(position);
        Log.d(TAG, "onBindViewHolder: " + item.toString());
        holder.tvStt.setText(String.valueOf(position + 1));
        holder.tvStt.setTag(item);
        holder.tvName.setText(item.getProductItem().getProductName());
        holder.tvCount.setText(String.valueOf(item.getCount()));
        holder.tvPrice.setText(formatMoney(item.getProductItem().getPrice() * item.getCount()));
    }
    @Override
    public int getItemCount() {
        return listInvoicesItem.size();
    }

    private String formatMoney(int totalMoney) {
        Locale locale = new Locale("vi", "VN");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(totalMoney);
    }

    public class ItemDataHolder extends RecyclerView.ViewHolder {
        TextView tvName, tvCount, tvPrice, tvStt;
        ImageView imRemove;

        public ItemDataHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.m004_product_name);
            tvCount = itemView.findViewById(R.id.m004_product_count);
            tvPrice = itemView.findViewById(R.id.m004_product_money);
            tvStt = itemView.findViewById(R.id.m004_stt);
            imRemove = itemView.findViewById(R.id.m004_remove_item_product);
            imRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int index = Integer.parseInt(tvStt.getText().toString()) - 1;
                    listInvoicesItem.remove(index);
                    notifyItemRemoved(index);
                }
            });
        }
    }

}
