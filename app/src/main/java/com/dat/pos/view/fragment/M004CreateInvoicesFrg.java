package com.dat.pos.view.fragment;

import android.app.DatePickerDialog;
import android.view.View;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.DialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dat.pos.R;
import com.dat.pos.model.Invoices;
import com.dat.pos.model.InvoicesItem;
import com.dat.pos.presenter.M004AddInvoicesPresenter;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.adapter.M004ProductAdapter;
import com.dat.pos.view.dialog.DialogInput;
import com.dat.pos.view.dialog.M004DialogPickProduct;
import com.google.android.material.snackbar.BaseTransientBottomBar;
import com.google.android.material.snackbar.Snackbar;

import java.util.Objects;

public class M004CreateInvoicesFrg extends BaseFragment<M004AddInvoicesPresenter> implements OnActionCallback {
    public static final String TAG = M004CreateInvoicesFrg.class.getName();
    private static final String SET_TITLE = "Thay đổi tên tiêu đề";
    private Invoices invoices;
    private M004ProductAdapter adapter;
    private RecyclerView recyclerView;
    private TextView tvTotalMoney, tvTime, tvID, tvTitle;
    private CardView cvAddTitle;
    private int mYear, mMonth, mDay;
    private M004DialogPickProduct dialogPickProduct;

    @Override
    protected void initView() {
        recyclerView = findViewById(R.id.m004_rv_product);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        findViewById(R.id.m004_back, this);
        tvTotalMoney = findViewById(R.id.m004_total_money);
        findViewById(R.id.pick_date, this);
        tvTime = findViewById(R.id.m004_tv_time);
        tvTitle = findViewById(R.id.m004_tv_title);
        tvID = findViewById(R.id.m004_add_id);
        cvAddTitle = findViewById(R.id.m004_add_title, this::onClick);
        findViewById(R.id.m004_add_product, this);
        findViewById(R.id.m004_done, this);
    }

    @Override
    protected M004AddInvoicesPresenter getPresenter() {
        return new M004AddInvoicesPresenter(this);
    }

    @Override
    protected void initData() {
        invoices = new Invoices();
        adapter = new M004ProductAdapter(this, invoices.getInvoicesItem(), mContext);
        recyclerView.setAdapter(adapter);
        tvID.setText(String.format("%05d", mPresenter.getCurrentID()));
        tvTotalMoney.setText(String.valueOf(mPresenter.calculateToTalMoney(invoices)));
        String date = mPresenter.getCurrentDate();
        String[] splitDate = date.split("/");
        mDay = Integer.parseInt(splitDate[0]);
        mMonth = Integer.parseInt(splitDate[1]);
        mYear = Integer.parseInt(splitDate[2]);
        tvTime.setText(mPresenter.getCurrentDate());
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.pick_date:
                pickDate();
                break;
            case R.id.m004_add_product:
                showDialog();
                break;
            case R.id.m004_back:
                getActivity().onBackPressed();
                break;
            case R.id.m004_done:
                setData();
                if (mPresenter.addInvoices(invoices)) {
                    getActivity().onBackPressed();
                } else {
                    Snackbar.make(mRootView, "Cần thêm thông tin", BaseTransientBottomBar.LENGTH_SHORT).show();
                }
                break;
            case R.id.m004_add_title:
                showDialogInput(SET_TITLE);
                break;
        }
    }

    private void setData() {
        invoices.setId(tvID.getText().toString());
        invoices.setName(Objects.requireNonNull(tvTitle.getText()).toString());
        invoices.setTerm(tvTime.getText().toString());
        invoices.setTimeCreated(mPresenter.getCurrentTime());
    }

    private void showDialogInput(String type) {
        DialogFragment dialogFragment = new DialogInput(this, type);
        dialogFragment.show(getFragmentManager(), type);
    }

    private void showDialog() {
        dialogPickProduct = M004DialogPickProduct.newInstance(this);
        dialogPickProduct.show(getFragmentManager(), "add product to invoices");

    }

    private void pickDate() {
        // Get Current Date

        DatePickerDialog datePickerDialog = new DatePickerDialog(mContext,
                (view, year, monthOfYear, dayOfMonth) -> tvTime.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year), mYear, mMonth, mDay);
        datePickerDialog.show();
    }


    @Override
    protected int getLayoutId() {
        return R.layout.m004_frg_create_invoice;
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case M004DialogPickProduct.ADD_PRODUCT:
                invoices.getInvoicesItem().add((InvoicesItem) data);
                adapter.notifyDataSetChanged();
                tvTotalMoney.setText(mPresenter.formatMoney(mPresenter.calculateToTalMoney(invoices)));
                break;
            case SET_TITLE:
                tvTitle.setText((String) data);
                break;
        }
    }
}
