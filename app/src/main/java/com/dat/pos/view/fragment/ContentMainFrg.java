package com.dat.pos.view.fragment;

import androidx.viewpager.widget.ViewPager;

import com.dat.pos.R;
import com.dat.pos.presenter.ContentMainPresenter;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.adapter.ContentMainAdapter;
import com.google.android.material.tabs.TabLayout;

public class ContentMainFrg extends BaseFragment<ContentMainPresenter> implements OnActionCallback {
    public static final String TAG = ContentMainFrg.class.getName();

    private ContentMainAdapter mainAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private M003HomeFrg m003HomeFrg;
    private M004InvoicesFrg m004InvoicesFrg;
    private M005ProfileFrg m005ProfileFrg;
    private M007StatisticFrg m007StatisticFrg;

    @Override
    protected void initView() {
        viewPager = findViewById(R.id.pager);
        tabLayout = findViewById(R.id.tab);
        mainAdapter = new ContentMainAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount(), mCallBack);
        viewPager.setOffscreenPageLimit(mainAdapter.getmNumOfTabs());
        viewPager.setAdapter(mainAdapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    @Override
    protected ContentMainPresenter getPresenter() {
        return new ContentMainPresenter(this);
    }


    @Override
    protected void initData() {
        m003HomeFrg = (M003HomeFrg) mainAdapter.getFragment(ContentMainAdapter.HOME_FRG);
        m004InvoicesFrg = (M004InvoicesFrg) mainAdapter.getFragment(ContentMainAdapter.INVOICES);
        m005ProfileFrg = (M005ProfileFrg) mainAdapter.getFragment(ContentMainAdapter.PROFILE);
        getFireBaseDatabaseHelp().loadUserData();
    }

    @Override
    public void callBack(String key, Object data) {
    }

    @Override
    protected int getLayoutId() {
        return R.layout.content_main;
    }
}
