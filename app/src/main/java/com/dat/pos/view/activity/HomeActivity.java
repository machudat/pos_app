package com.dat.pos.view.activity;

import android.widget.Toast;

import com.dat.pos.App;
import com.dat.pos.R;
import com.dat.pos.view.fragment.ContentMainFrg;
import com.dat.pos.view.fragment.M001LoginFrg;
import com.dat.pos.view.fragment.M002RegisterFrg;
import com.dat.pos.view.fragment.M003HomeFrg;
import com.dat.pos.view.fragment.M004CreateInvoicesFrg;
import com.dat.pos.view.fragment.M004EditInvoicesFrg;
import com.dat.pos.view.fragment.M004InvoicesFrg;
import com.dat.pos.view.fragment.M005ProfileFrg;
import com.dat.pos.view.fragment.M006AddProductFrg;
import com.dat.pos.view.fragment.M006AllProductFrg;
import com.dat.pos.view.fragment.M008CategorizeFrg;
import com.google.android.material.tabs.TabLayout;

import java.util.EmptyStackException;

public class HomeActivity extends BaseActivity {
    private static final String TAG = HomeActivity.class.getName();
    private TabLayout tabLayout;

    @Override
    protected void initView() {
        //showFrg(M001LoginFrg.TAG, R.id.linear_home);
        if (App.getInstance().getFirebaseDatabase().getmUser() == null) {
            showFrg(M001LoginFrg.TAG, R.id.linear_home);
            return;
        }
        showFrg(ContentMainFrg.TAG, R.id.linear_home);


    }
    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    public void callBack(String key, Object data) {
        switch (key) {
            case M001LoginFrg.REGISTER:
                showFrg(M002RegisterFrg.TAG, R.id.linear_home);
                break;
            case M002RegisterFrg.MOVE_TO_LOGIN:
                Toast.makeText(this, "hello", Toast.LENGTH_SHORT).show();
                showFrg(M001LoginFrg.TAG, R.id.linear_home);
                break;
            case M001LoginFrg.LOGIN_SUCESS:
                showFrg(ContentMainFrg.TAG, R.id.linear_home);
                break;
            case M003HomeFrg.KEY_MOVE_TO_PRODUCT:
                addFrg(M006AllProductFrg.TAG, R.id.linear_home);
                break;
            case M006AllProductFrg.KEY_ADD_PRODUCT:
                addFrg(M006AddProductFrg.TAG, R.id.linear_home);
                break;
            case M005ProfileFrg.KEY_SIGN_OUT_SUCCESS:
                showFrg(M001LoginFrg.TAG, R.id.linear_home);
                break;
            case M003HomeFrg.MOVE_TO_CATEGORY:
                addFrg(M008CategorizeFrg.TAG, R.id.linear_home);
                break;
            case M004InvoicesFrg.SHOW_ADD_NEW_INVOICES:
                addFrg(M004CreateInvoicesFrg.TAG, R.id.linear_home);
                break;
            case M004InvoicesFrg.SHOW_EDIT_INVOICES:
                addFrg(M004EditInvoicesFrg.TAG, R.id.linear_home);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        try {
            removeCurrentFrg();
        } catch (EmptyStackException e) {
            super.onBackPressed();
        }

    }
}