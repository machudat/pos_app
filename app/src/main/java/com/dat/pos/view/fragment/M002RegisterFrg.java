package com.dat.pos.view.fragment;

import android.view.View;
import android.widget.Toast;

import com.dat.pos.R;
import com.dat.pos.presenter.M002RegisterPresenter;
import com.dat.pos.ultils.FireBaseDatabaseHelp;
import com.dat.pos.view.Callback.OnActionCallback;
import com.google.android.material.textfield.TextInputEditText;

public class M002RegisterFrg extends BaseFragment<M002RegisterPresenter> implements OnActionCallback {
    public static final String TAG = M002RegisterFrg.class.getName();
    public static final String MOVE_TO_LOGIN = "đăng nhập";
    private TextInputEditText nameEditText, phoneNumberEditText, emailEditText, passwordEditText;

    @Override
    protected M002RegisterPresenter getPresenter() {
        return new M002RegisterPresenter(this);
    }

    @Override
    protected void initData() {

    }

    @Override
    protected int getLayoutId() {
        return R.layout.m002_frg_register;
    }

    @Override
    protected void initView() {
        nameEditText = findViewById(R.id.edt_register_name);
        phoneNumberEditText = findViewById(R.id.edt_register_phone_number);
        emailEditText = findViewById(R.id.edt_register_email);
        passwordEditText = findViewById(R.id.edt_register_password);
        findViewById(R.id.bt_register, this);
        findViewById(R.id.tv_sign_in, this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()) {
            case R.id.bt_register:
                register();
                break;
            case R.id.tv_sign_in:
                mCallBack.callBack(MOVE_TO_LOGIN, null);

                break;
        }
    }

    @Override
    public void callBack(String key, Object data) {
        Toast.makeText(mContext, key, Toast.LENGTH_LONG).show();
        switch (key) {
            case FireBaseDatabaseHelp.SUCCESS:
                Toast.makeText(mContext, "Đăng ký thành công", Toast.LENGTH_SHORT).show();
                mCallBack.callBack(MOVE_TO_LOGIN, null);
                break;
            case FireBaseDatabaseHelp.FAIL:
                Toast.makeText(mContext, "Đăng ký thất bại", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void register() {
        String email = emailEditText.getText().toString().trim();
        String name = nameEditText.getText().toString().trim();
        String phoneNumberr = phoneNumberEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        if (!mPresenter.checkRegister(name, phoneNumberr, email, password)) {
            Toast.makeText(mContext, "Đăng ký thông tin không đúng định dạng", Toast.LENGTH_LONG).show();
            return;
        }
        int phoneNumber = 0000000000;
        try {
            phoneNumber = Integer.parseInt(phoneNumberr);
        } catch (Exception NumberFormatException) {
            Toast.makeText(mContext, "Sai số điện thoại", Toast.LENGTH_SHORT).show();
            return;
        }

        getFireBaseDatabaseHelp().signUp(this, name, phoneNumber, email, password);
    }
}
