package com.dat.pos.presenter;

import com.dat.pos.model.ProductItem;
import com.dat.pos.view.Callback.OnActionCallback;

public class M006AllProductPresenter extends BasePresenter<OnActionCallback> {

    public M006AllProductPresenter(OnActionCallback mCallback) {
        super(mCallback);
    }

    public void update(ProductItem data, OnActionCallback callback) {
        getFireBaseDatabaseHelp().updateProduct(data, callback);
    }

    public boolean removeItem(ProductItem productItem) {
        getFireBaseDatabaseHelp().removeItemProduct(productItem);
        return true;
    }
}
