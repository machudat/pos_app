package com.dat.pos.presenter;

import com.dat.pos.view.Callback.OnActionCallback;

public class M002RegisterPresenter extends BasePresenter<OnActionCallback> {

    public M002RegisterPresenter(OnActionCallback mCallback) {

        super(mCallback);
    }

    public boolean checkRegister(String name, String phoneNumber, String email, String password) {
        boolean kq = true;
        try {
            Integer.parseInt(phoneNumber);
            if (name.isEmpty() || phoneNumber.length() < 8 || email.isEmpty() || password.isEmpty()) {
                kq = false;
            }

        } catch (NumberFormatException e) {
            kq = false;
            throw new NumberFormatException("not phone");
        }
        return kq;
    }
}
