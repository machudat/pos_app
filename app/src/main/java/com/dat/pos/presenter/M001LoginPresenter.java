package com.dat.pos.presenter;

import com.dat.pos.view.Callback.OnActionCallback;

public class M001LoginPresenter extends BasePresenter<OnActionCallback> {

    public M001LoginPresenter(OnActionCallback mCallback) {
        super(mCallback);
    }

    public boolean checkLogin(String email, String password) {
        return !email.isEmpty() && !password.isEmpty();

    }
}
