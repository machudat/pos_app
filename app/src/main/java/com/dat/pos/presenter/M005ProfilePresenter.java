package com.dat.pos.presenter;

import com.dat.pos.view.Callback.OnActionCallback;

import java.util.Calendar;

public class M005ProfilePresenter extends BasePresenter<OnActionCallback> {

    public M005ProfilePresenter(OnActionCallback mCallback) {
        super(mCallback);
    }

    public String getCurrentDate() {
        final Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);
    }
}
