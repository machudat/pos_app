package com.dat.pos.presenter;

import com.dat.pos.model.Invoices;
import com.dat.pos.model.InvoicesItem;
import com.dat.pos.view.Callback.OnActionCallback;

import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class M004InvoicesPresenter extends BasePresenter<OnActionCallback> {
    public M004InvoicesPresenter(OnActionCallback mCallback) {
        super(mCallback);
    }

    public int calculateTodayIncome(List<Invoices> invoices) {
        int totalMoney = 0;
        String currentDate = getCurrentDate();
        for (Invoices item : invoices) {
            if (getDate(item.getTimeCreated()).equals(currentDate)) {
                totalMoney += calculateToTalMoney(item);
            }
        }

        if (totalMoney <= 0) {
            return 0;
        }
        return totalMoney;
    }

    private String getDate(String timeCreated) {
        return timeCreated.substring(0, timeCreated.indexOf(" "));
    }

    public int calculateToTalMoney(Invoices invoices) {
        int totalMoney = 0;
        if (!(invoices.getInvoicesItem().isEmpty() || invoices == null)) {
            for (InvoicesItem i : invoices.getInvoicesItem()) {
                totalMoney += i.getCount() * i.getProductItem().getPrice();
            }
        }

        if (totalMoney <= 0) {
            return 0;
        }
        return totalMoney;
    }

    public String formatMoney(int totalMoney) {
        Locale locale = new Locale("vi", "VN");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(totalMoney);
    }


    public String getCurrentDate() {
        final Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);
    }

    public String calculateInvoicesLeft(List<Invoices> data) {
        int count = 0;
        for (Invoices item : data) {
            if (compare(item.getTerm(), getCurrentDate())) {
                count++;
            }
        }
        return String.valueOf(count);
    }

    private boolean compare(String term, String currentDate) {
        String[] arrTerm = term.split("/");
        String[] arrCurrentDate = currentDate.split("/");
        return Integer.parseInt(arrTerm[2]) >= Integer.parseInt(arrCurrentDate[2])
                && Integer.parseInt(arrTerm[1]) >= Integer.parseInt(arrCurrentDate[1])
                && Integer.parseInt(arrTerm[0]) >= Integer.parseInt(arrCurrentDate[0]);
    }

}
