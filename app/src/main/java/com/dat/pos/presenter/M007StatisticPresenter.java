package com.dat.pos.presenter;

import android.os.Build;
import android.util.Log;

import androidx.annotation.RequiresApi;

import com.anychart.chart.common.dataentry.DataEntry;
import com.anychart.chart.common.dataentry.ValueDataEntry;
import com.dat.pos.model.Invoices;
import com.dat.pos.model.InvoicesItem;
import com.dat.pos.view.Callback.OnActionCallback;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class M007StatisticPresenter extends BasePresenter<OnActionCallback> {
    private static final String TAG = M007StatisticPresenter.class.getName();

    public M007StatisticPresenter(OnActionCallback mCallback) {
        super(mCallback);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<DataEntry> getDataTemplatePie(List<Invoices> invoicesList) {
        List<DataEntry> data = new ArrayList<>();
        Map<String, Integer> dataTest = new HashMap<>();
        for (Invoices item : invoicesList) {
            List<InvoicesItem> invoicesItem = item.getInvoicesItem();
            for (InvoicesItem invoiceItem : invoicesItem) {
                int total = invoiceItem.getProductItem().getPrice() * invoiceItem.getCount();
                String name = invoiceItem.getProductItem().getProductName();
                if (dataTest.get(name) == null) {
                    dataTest.put(name, total);
                    continue;
                }
                dataTest.put(name, dataTest.get(name) + total);
            }
        }
        for (Map.Entry<String, Integer> entry : dataTest.entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue();
            data.add(new ValueDataEntry(key, value));
            // do something with key and/or tab
        }
//        dataTest.forEach((key, value) -> {
//            data.add(new ValueDataEntry(key, value));
//        });
        return data;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public List<DataEntry> getDateTemplateColumn(List<DataEntry> dataCartesian, List<Invoices> listInvoices) {
        List<DataEntry> data = dataCartesian;
        Map<String, Integer> dataTest = initMap();
        for (int i = 0; i < listInvoices.size(); i++) {
            String timeCreated = getDate(listInvoices.get(i).getTimeCreated()).substring(0, listInvoices.get(i).getTimeCreated().lastIndexOf("/"));
            Log.d(TAG, "getDateTemplateColumn: " + timeCreated);
            Log.d(TAG, "getDateTemplateColumn: " + dataTest.get(timeCreated));

            if (dataTest.get(timeCreated) != null) {
                dataTest.put(timeCreated, calculateToTalMoney(listInvoices.get(i)) + dataTest.get(timeCreated));
            }
        }

        for (Map.Entry<String, Integer> entry : dataTest.entrySet()) {
            String key = entry.getKey();
            int value = entry.getValue();
            data.add(new ValueDataEntry(key, value));
            // do something with key and/or tab
        }
//        dataTest.forEach((key, value) -> {
//            Log.d(TAG, "getDateTemplateColumn: "+key + "  " + value);
//            data.add(new ValueDataEntry(key, value));
//        });
        return data;
    }

    private Map<String, Integer> initMap() {
        Map<String, Integer> data = new HashMap<>();
        data.put(calDays(6), 0);
        data.put(calDays(5), 0);
        data.put(calDays(4), 0);
        data.put(calDays(3), 0);
        data.put(calDays(2), 0);
        data.put(calDays(1), 0);
        data.put(calDays(0), 0);
        return data;
    }

    private String calDays(int i) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_MONTH, -i);
        Date date = c.getTime();
        DateFormat dateFormat = new SimpleDateFormat("dd/MM");
        String strDate = dateFormat.format(date);
        return strDate;
    }

    public String getCurrentDate() {
        final Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);
    }

    public int calculateToTalMoney(Invoices invoices) {
        int totalMoney = 0;
        if (!(invoices.getInvoicesItem().isEmpty() || invoices == null)) {
            for (InvoicesItem i : invoices.getInvoicesItem()) {
                totalMoney += i.getCount() * i.getProductItem().getPrice();
            }
        }

        if (totalMoney <= 0) {
            return 0;
        }
        return totalMoney;
    }

    private String getDate(String timeCreated) {
        return timeCreated.substring(0, timeCreated.indexOf(" "));
    }

}
