package com.dat.pos.presenter;

import com.dat.pos.App;
import com.dat.pos.model.Invoices;
import com.dat.pos.model.InvoicesItem;
import com.dat.pos.model.ProductItem;
import com.dat.pos.view.Callback.OnActionCallback;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class M004AddInvoicesPresenter extends BasePresenter<OnActionCallback> {
    private static final String TAG = M004AddInvoicesPresenter.class.getName();

    public M004AddInvoicesPresenter(OnActionCallback mCallback) {
        super(mCallback);
    }

    public boolean addInvoices(Invoices invoices) {
        try {
            if (invoices.getInvoicesItem().isEmpty() || invoices.getName().isEmpty()) {
                return false;
            }
            for (int i = 0; i < invoices.getInvoicesItem().size(); i++) {
                ProductItem data = invoices.getInvoicesItem().get(i).getProductItem();
                ProductItem dataAdd = new ProductItem(data.getProductName(), data.getPrice(), data.getNote(), data.getImagePath(), data.getNameCategory());
                invoices.getInvoicesItem().get(i).setProductItem(dataAdd);
            }
        } catch (NullPointerException e) {
            return false;
        }
        getFireBaseDatabaseHelp().addNewInvoices(invoices);

        return true;
    }

    public int calculateToTalMoney(Invoices invoices) {
        int totalMoney = 0;
        if (!(invoices.getInvoicesItem().isEmpty() || invoices == null)) {
            for (InvoicesItem i : invoices.getInvoicesItem()) {
                totalMoney += i.getCount() * i.getProductItem().getPrice();
            }
        }

        if (totalMoney <= 0) {
            return 0;
        }
        return totalMoney;
    }

    public String formatMoney(int totalMoney) {
        Locale locale = new Locale("vi", "VN");
        NumberFormat numberFormat = NumberFormat.getCurrencyInstance(locale);
        return numberFormat.format(totalMoney);
    }

    public String getCurrentTime() {
        return new SimpleDateFormat("dd/MM/yyyy HH:mm").format(new Date());
    }

    public String getCurrentDate() {
        final Calendar c = Calendar.getInstance();
        return c.get(Calendar.DAY_OF_MONTH) + "/" + (c.get(Calendar.MONTH) + 1) + "/" + c.get(Calendar.YEAR);
    }

    public int getCurrentID() {
        List<Invoices> invoicesList = App.getInstance().getStorage().getListInvoices().getValue();
        List<Integer> listID = new ArrayList<>();
        try {
            if (invoicesList.isEmpty()) {
                return 1;
            }
        } catch (NullPointerException e) {
            return 1;
        }

        for (Invoices invoices : invoicesList) {
            listID.add(Integer.parseInt(invoices.getId()));
        }
        return Collections.max(listID) + 1;
    }


    public boolean update(Invoices invoices) {
        try {
            if (invoices.getInvoicesItem().isEmpty() || invoices.getName().isEmpty()) {
                return false;
            }
            for (int i = 0; i < invoices.getInvoicesItem().size(); i++) {
                ProductItem data = invoices.getInvoicesItem().get(i).getProductItem();
                ProductItem dataAdd = new ProductItem(data.getProductName(), data.getPrice(), data.getNote(), data.getImagePath(), data.getNameCategory());
                invoices.getInvoicesItem().get(i).setProductItem(dataAdd);
            }
        } catch (NullPointerException e) {
            return false;
        }
        getFireBaseDatabaseHelp().updateInvoices(invoices, mCallback);

        return true;
    }
}
