package com.dat.pos.presenter;

import com.dat.pos.model.CategoryProduct;
import com.dat.pos.view.Callback.OnActionCallback;

import java.util.List;

public class M008CategorizePresenter extends BasePresenter<OnActionCallback> {
    public M008CategorizePresenter(OnActionCallback mCallback) {
        super(mCallback);
    }


    public boolean checkAddCategory(String data, List<CategoryProduct> listCategory) {
        if (listCategory == null) {
            return true;
        }
        for (CategoryProduct category : listCategory) {
            if (category.getName().equals(data)) ;
            return false;
        }
        return true;
    }
}
