package com.dat.pos.presenter;

import android.net.Uri;

import com.dat.pos.App;
import com.dat.pos.model.CategoryProduct;
import com.dat.pos.view.Callback.OnActionCallback;

public class M007AddProductPresenter extends BasePresenter<OnActionCallback> {

    public static final String MORE_INFORMATION = "thiếu thông tin";
    public static final String ADD_IF = "ADD IF";

    public M007AddProductPresenter(OnActionCallback mCallback) {
        super(mCallback);
    }

    public boolean addProduct(String category, String name, String price, String note, Uri uri) {
        int price1 = 0;
        try {
            price1 = Integer.parseInt(price);
            if (name.isEmpty() || price1 < 1000) {
                return false;
            }
        } catch (Exception NumberFormatException) {
            return false;
        }
        String key = "";
        try {
            for (CategoryProduct item : App.getInstance().getStorage().getListCategory().getValue()) {
                if (category.equals(item.getName())) {
                    key = item.getKey();
                }
            }
        } catch (NullPointerException e) {
            return false;
        }

        getFireBaseDatabaseHelp().addNewProduct(key, name, price1, note, uri);
        return true;
    }
}
