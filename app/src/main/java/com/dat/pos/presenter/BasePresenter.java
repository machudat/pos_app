package com.dat.pos.presenter;

import com.dat.pos.App;
import com.dat.pos.ultils.FireBaseDatabaseHelp;
import com.dat.pos.view.Callback.OnActionCallback;

public class BasePresenter<T extends OnActionCallback> {
    protected T mCallback;

    public BasePresenter(T mCallback) {
        this.mCallback = mCallback;
    }

    public FireBaseDatabaseHelp getFireBaseDatabaseHelp() {
        return App.getInstance().getFirebaseDatabase();
    }
}
