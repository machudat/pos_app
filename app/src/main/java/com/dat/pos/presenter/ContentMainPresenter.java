package com.dat.pos.presenter;

import com.dat.pos.view.Callback.OnActionCallback;

public class ContentMainPresenter extends BasePresenter<OnActionCallback> {
    public ContentMainPresenter(OnActionCallback mCallback) {
        super(mCallback);
    }
}
