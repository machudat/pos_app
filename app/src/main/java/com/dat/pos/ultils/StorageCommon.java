package com.dat.pos.ultils;

import androidx.lifecycle.MutableLiveData;

import com.dat.pos.model.CategoryProduct;
import com.dat.pos.model.Invoices;
import com.dat.pos.model.ProductItem;
import com.dat.pos.model.UserInformationItem;

import java.util.List;

public final class StorageCommon {
    private MutableLiveData<UserInformationItem> userInformation;
    private final MutableLiveData<List<CategoryProduct>> listCategory;
    private MutableLiveData<List<ProductItem>> listProduct;
    private final MutableLiveData<List<Invoices>> listInvoices;
    private Invoices m004Invoices;

    public StorageCommon() {
        userInformation = new MutableLiveData<>();
        listProduct = new MutableLiveData<>();
        listCategory = new MutableLiveData<>();
        listInvoices = new MutableLiveData<>();
        m004Invoices = new Invoices();
    }

    public Invoices getM004Invoices() {
        return m004Invoices;
    }

    public void setM004Invoices(Invoices m004Invoices) {
        this.m004Invoices = m004Invoices;
    }

    public MutableLiveData<UserInformationItem> getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformationItem data) {
        userInformation.setValue(data);
    }

    public MutableLiveData<List<ProductItem>> getListProduct() {
        return listProduct;
    }

    public void setUserInformation(MutableLiveData<UserInformationItem> userInformation) {
        this.userInformation = userInformation;
    }

    public void setListProduct(MutableLiveData<List<ProductItem>> listProduct) {
        this.listProduct = listProduct;
    }

    public MutableLiveData<List<CategoryProduct>> getListCategory() {
        return listCategory;
    }

    public void setListCategory(List<CategoryProduct> listCategory) {
        this.listCategory.setValue(listCategory);
    }

    public MutableLiveData<List<Invoices>> getListInvoices() {
        return listInvoices;
    }

    public void setListInvoices(List<Invoices> listInvoices) {
        this.listInvoices.setValue(listInvoices);
    }
}

