package com.dat.pos.ultils;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.dat.pos.App;
import com.dat.pos.model.CategoryProduct;
import com.dat.pos.model.Invoices;
import com.dat.pos.model.ProductItem;
import com.dat.pos.model.UserInformationItem;
import com.dat.pos.view.Callback.OnActionCallback;
import com.dat.pos.view.fragment.M005ProfileFrg;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

public class FireBaseDatabaseHelp {
    public static final String REMOVE_PRODUCT = "remove product";
    private static final String TAG = FireBaseDatabaseHelp.class.getName();

    public static final String FAIL = "0";
    public static final String SUCCESS = "1";
    public static final String LOAD_USER_INFORMATION = "load user data complete";
    public static final String LOAD_USER_PRODUCT_COMPLETE = "load user product complete";

    private static final String LINK_ACCOUNT_PROFILE = "/profile";
    private static final String LINK_ACCOUNT_PRODUCT = "/product";
    private static final String LINK_ACCOUNT_PRODUCT_ITEM = "/ListProduct";
    private static final String LINK_ACCOUNT_CATEGORY = "/category";
    public static final String ADD_NEW_PRODUCT = "add new product";
    public static final String UPDATE_SUCCESS = "UPDATE SUCCESS";
    private static final String LINK_ACCOUNT_INVOICES = "/invoices";


    private FirebaseDatabase mDatabase;
    private DatabaseReference mReference;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseStorage mFirebaseStorage;
    private StorageReference mStorageRef;


    private String defaultStorageAvatar;

    public FireBaseDatabaseHelp() {
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance();
        mReference = mDatabase.getReference();

        mFirebaseStorage = FirebaseStorage.getInstance();
        mStorageRef = mFirebaseStorage.getReference();
        defaultStorageAvatar = "/defaultA.jpg";

    }

    public static String getTAG() {
        return TAG;
    }

    //(String productName, int price, String note, String ImagePath)
    public void updateProduct(ProductItem data, OnActionCallback callback) {
        String link = mUser.getUid() + LINK_ACCOUNT_CATEGORY + "/" + data.getKeyCategory() + LINK_ACCOUNT_PRODUCT_ITEM + "/" + data.getKey();
        ProductItem dataAdd = new ProductItem(data.getProductName(), data.getPrice(), data.getNote(), data.getImagePath());
        update(dataAdd, callback, link);
    }

    public void updateInvoices(Invoices data, OnActionCallback callback) {
        String link = mUser.getUid() + LINK_ACCOUNT_INVOICES + "/" + data.getKey();
        update(data, callback, link);
    }

    private void update(Object data, OnActionCallback callback, String link) {
        Invoices dataAdd = (Invoices) data;
        dataAdd.setKey(null);
        mReference.child(link).setValue(data).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                callback.callBack(UPDATE_SUCCESS, data);
            }
        });
    }

    public void addNewCategory(String name) {
        String key = mReference.push().getKey();
        CategoryProduct categoryProduct = new CategoryProduct(name);
        categoryProduct.setImagePath("defaultCategory.jpeg");
        mReference.child(mUser.getUid() + LINK_ACCOUNT_CATEGORY + "/" + key).setValue(categoryProduct).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d(TAG, "add category success ");
            }
        });
    }

    public void removeItemInvoices(Invoices invoices) {
        Log.d(TAG, "removeItemInvoices: " + mReference.child(mUser.getUid() + LINK_ACCOUNT_INVOICES + "/" + invoices.getKey()).toString());
        mReference.child(mUser.getUid() + LINK_ACCOUNT_INVOICES + "/" + invoices.getKey()).removeValue();
    }

    public void removeItemCategory(CategoryProduct categoryProduct) {
        mReference.child(mUser.getUid() + LINK_ACCOUNT_CATEGORY + "/" + categoryProduct.getKey()).removeValue();
    }

    public void removeItemProduct(ProductItem productItem) {
        mReference.child(getmUser().getUid() + LINK_ACCOUNT_CATEGORY + "/" + productItem.getKeyCategory()
                + LINK_ACCOUNT_PRODUCT_ITEM + "/" + productItem.getKey()).removeValue();
    }

    public void loadUserData() {
        //Load category
        mReference.child(mUser.getUid() + LINK_ACCOUNT_CATEGORY).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<CategoryProduct> listCategoryProduct = new ArrayList<>();
                for (DataSnapshot data : snapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: " + data);
                    CategoryProduct item = new CategoryProduct();
                    item.setKey(data.getKey());
                    item.setName(data.child("name").getValue(String.class));
                    item.setImagePath(data.child("imagePath").getValue(String.class));
                    List<ProductItem> itemList = new ArrayList<>();
                    for (DataSnapshot data2 : data.child(LINK_ACCOUNT_PRODUCT_ITEM).getChildren()) {
                        Log.d(TAG, "onDataChange: " + data2.getValue(ProductItem.class).getProductName());
                        ProductItem temp = data2.getValue(ProductItem.class);
                        temp.setKey(data2.getKey());
                        temp.setNameCategory(item.getName());
                        temp.setKeyCategory(item.getKey());
                        itemList.add(temp);
                    }
                    item.setListProduct(itemList);
                    listCategoryProduct.add(item);
                }
                getStore().setListCategory(listCategoryProduct);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        //load invoices
        mReference.child(mUser.getUid() + LINK_ACCOUNT_INVOICES).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<Invoices> invoicesList = new ArrayList<>();
                for (DataSnapshot data : snapshot.getChildren()) {
                    Log.d(TAG, "onDataChange: " + data);
                    Invoices item = data.getValue(Invoices.class);
                    item.setKey(data.getKey());
                    invoicesList.add(item);
                }
                getStore().setListInvoices(invoicesList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        //load profile
        mReference.child(mUser.getUid() + LINK_ACCOUNT_PROFILE).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                UserInformationItem data = snapshot.getValue(UserInformationItem.class);
                getStore().setUserInformation(data);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

    }

    public void login(OnActionCallback mCallback, String email, String password) {
        Log.d(TAG, "login: verify" + email);
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    mUser = mAuth.getCurrentUser();
                    mAuth = FirebaseAuth.getInstance();
                    mUser = mAuth.getCurrentUser();
                    mDatabase = FirebaseDatabase.getInstance();
                    mReference = mDatabase.getReference();
                    mFirebaseStorage = FirebaseStorage.getInstance();
                    mStorageRef = mFirebaseStorage.getReference();
                    Log.d(TAG, "onComplete: loginSuccess");
                    loadUserData();
                    mCallback.callBack(SUCCESS, null);
                } else {
                    mCallback.callBack(FAIL, null);
                }
            }
        });
    }


    public void downImage(String imagePath, ImageView imTopicImage, Context context) {
        mStorageRef.child(imagePath).getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Glide.with(context).load(uri).into(imTopicImage);
            }
        });
    }

    public void addNewInvoices(Invoices invoices) {
        String key = mReference.push().getKey();
        String url = mAuth.getUid() + LINK_ACCOUNT_INVOICES + "/" + key;
        mReference.child(url).setValue(invoices).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Toast.makeText(App.getInstance().getApplicationContext(), "Thành công", Toast.LENGTH_SHORT).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(App.getInstance().getApplicationContext(), "Thất bại", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void addNewProduct(String keyCategory, String name, int price, String note, Uri uri) {
        String key = mReference.push().getKey();
        String linkImg = "";
        if (uri != null) {
            linkImg = (mAuth.getUid() + "/product/" + key);
        } else {
            linkImg = defaultStorageAvatar;
        }
        ProductItem item = new ProductItem(name, price, note, linkImg);
        mReference.child(mAuth.getUid() + LINK_ACCOUNT_CATEGORY + "/" + keyCategory + "/" + LINK_ACCOUNT_PRODUCT_ITEM + "/" + key).setValue(item).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
            }
        });
        Log.d(TAG, "addNewProduct: " + uri);
        if (uri == null) {
            return;
        }
        putImg(linkImg, uri);

    }

    private void putImg(String linkImg, Uri uri) {
        StorageReference reference = mStorageRef.child(linkImg);
        reference.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(App.getInstance().getApplicationContext(), "Failed " + e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void signUp(OnActionCallback mCallBack, String name, int phoneNumber, String email, String password) {
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    mUser = mAuth.getCurrentUser();
                    UserInformationItem userInfo = new UserInformationItem(name, phoneNumber, email, password, defaultStorageAvatar);
                    mReference.child(mUser.getUid() + LINK_ACCOUNT_PROFILE).setValue(userInfo);
                    mCallBack.callBack(SUCCESS, userInfo);
                } else {
                    mCallBack.callBack(FAIL, null);

                }
            }
        });
    }

    public void logout(OnActionCallback mCallBack) {
        mAuth.signOut();
        mCallBack.callBack(M005ProfileFrg.KEY_SIGN_OUT_SUCCESS, null);
    }

    public FirebaseDatabase getmDatabase() {
        return mDatabase;
    }

    public DatabaseReference getmReference() {
        return mReference;
    }

    public FirebaseAuth getmAuth() {
        return mAuth;
    }

    public FirebaseUser getmUser() {
        return mUser;
    }

    public FirebaseStorage getmFirebaseStorage() {
        return mFirebaseStorage;
    }

    public StorageReference getmStorageRef() {
        return mStorageRef;
    }

    public String getDefaultStorageAvatar() {
        return defaultStorageAvatar;
    }


    public void setDefaultStorageAvatar(String defaultStorageAvatar) {
        this.defaultStorageAvatar = defaultStorageAvatar;
    }

    private StorageCommon getStore() {
        return App.getInstance().getStorage();
    }


    public void saveProductInformation(Object data) {
    }


    public void updateProfile(UserInformationItem userInformationItem) {
        mReference.child(mUser.getUid() + LINK_ACCOUNT_PROFILE).setValue(userInformationItem).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                Log.d(TAG, "onComplete: Cập nhật thông tin cá nhân thành công");
            }
        });
    }


}
