package com.dat.pos.presenter;

import com.dat.pos.model.Invoices;
import com.dat.pos.model.InvoicesItem;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class M004AddInvoicesPresenterTest {
    private M004AddInvoicesPresenter m004AddInvoicesPresenter;
    private List<InvoicesItem> imInvoicesItems;

    // public InvoicesItem(ProductItem productItem, int count) {
    @Before
    public void setUp() throws Exception {
        m004AddInvoicesPresenter = new M004AddInvoicesPresenter(null);

    }

    // public Invoices(List<InvoicesItem> invoicesItem, String id, String timeCreated, String term, String name, Customer customer) {
    @Test
    public void addInvoices() {
        assertEquals(false, m004AddInvoicesPresenter.addInvoices(null));
        assertEquals(false, m004AddInvoicesPresenter.addInvoices(new Invoices()));
    }

    @After
    public void teardown() throws Exception {

    }
}