package com.dat.pos.presenter;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class M002RegisterPresenterTest {
    M002RegisterPresenter m002RegisterPresenter = new M002RegisterPresenter(null);

    @Test
    public void checkRegister() {
        assertFalse(m002RegisterPresenter.checkRegister("", "0123412323", "trandat728@gmail.com", "trandat"));
        assertFalse(m002RegisterPresenter.checkRegister("", "0344822735", "trandat728@gmail.com", "trandat"));
        assertTrue(m002RegisterPresenter.checkRegister("Tran dat", "0344822735", "trandat728@gmail.com", "trandat"));
    }

    @Test
    public void testRegisterNumberformat() {
        try {
            assertTrue(m002RegisterPresenter.checkRegister("Tran dat", "03448hfgfghfghfge22735", "trandat728@gmail.com", "trandat"));
        } catch (Exception e) {
            Assert.assertTrue(e instanceof NumberFormatException);
        }
    }
}