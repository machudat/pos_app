package com.dat.pos.presenter;

import com.dat.pos.App;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.assertFalse;

@Config(maxSdk = 23, application = App.class)
@RunWith(RobolectricTestRunner.class)
public class M007AddProductPresenterTest {
    M007AddProductPresenter presenter = new M007AddProductPresenter(null);

    @Test
    public void addProduct() {
        assertFalse(presenter.addProduct("", "", "", "", null));
        assertFalse(presenter.addProduct("", "", "", "note", null));
        assertFalse(presenter.addProduct("", "dat", "", "note", null));
        assertFalse(presenter.addProduct("", "dat", "100000", "note", null));
        assertFalse(presenter.addProduct("test", "dattuyu", "10000", "note", null));
        assertFalse(presenter.addProduct("no", "dat", "100", "note", null));


    }
}